/**  @module includes/cli */

/**
 * This function returns the value of given parameter in command line.<br>
 * Name of parameter in CLI is used without any additional dashes etc.<br><br>
 *
 * @example nightwatch -t scriptName.js parameterName parameterValue parameterName2 parameterValue2
 *
 * @function
 * @requires /includes/cli.js
 * @author  R.Shamborovskyi
 * @param {String} paramName - The name of CLI parameter
 * @returns {String} Parameter value (false if not found)
 */
function getParam(paramName) {
	var nextIsRequiredParam = false;
	for(var i in process.argv) {
		if(process.argv[i] == paramName) {
			nextIsRequiredParam = true;
		} else if(nextIsRequiredParam) {
			return process.argv[i];
		}
	}
	return false;
}

module.exports.getParam = getParam;