/** @module includes/coverage */

/**
 * This function will add JS file to coverage checking procedure.
 *
 * @function
 * @param {Object} browser - nightwatch browser
 * @param {Array} jsFilesPaths - target JS files paths, or string "all" for all possible JS files in project
 * @param {Function} callback - will be called when all async logic is done
 */
function addFilesToCover(browser, jsFilesPaths, basePath, callback) {

    browser
    .pause(2500)
    .execute(function(jsFilesPaths, basePath) {

        jQuery('#questionnaire').html('');

        window.NIGHTWATCH_WHOLE_BODY = jQuery('body').html();

        jQuery('body').html('');

        if(jsFilesPaths == "all") {

            jsFilesPaths = [
                // 'combodate.js',
                'contexts.js',
                'helpers.js',
                'objects.js',
                'questionnaire.js',
                'services.js',
                'support.js',
                // 'umask.js',
                // 'upload.js',
                // 'xdr.js',
                // 'xmoment.js',
            ];
        }

        var filesToAppend = jsFilesPaths.length;
        var filesAppended = 0;

        var filePath = "plugins/questionnaire-publisher-0.3-SNAPSHOT/js/publisher/"
        var FE_basePath = basePath;
        var scr = document.createElement('script');
        scr.setAttribute('src','https://cdnjs.cloudflare.com/ajax/libs/blanket.js/1.1.4/blanket.js');
        var tmp = [];

        scr.onload = function() {

            for(var i in jsFilesPaths) {
                var url = FE_basePath+filePath+jsFilesPaths[i];
                tmp.push(url);

                var content =  jQuery.ajax({
                    type: "GET",
                    url: url,
                    async: false
                }).responseText;

                blanket.instrument(
                    {
                        inputFile: content,
                        inputFileName: url
                    },
                    function (instrumented) {
                        jQuery('<script>'+instrumented+'</script>').appendTo('head');
                        filesAppended++;

                        if(filesToAppend == filesAppended) {
                            jQuery('*').off();
                            jQuery('body').html(window.NIGHTWATCH_WHOLE_BODY);

                            // Remove datepicker container. It will be created
                            // again automatically when needed.
                            jQuery('#ui-datepicker-div').remove();

                            // jQuery('#questionnaire').questionnaire('initEvent');
                        }
                    }
                );
            }
        };

        document.head.appendChild(scr);

        return true;
    }, [jsFilesPaths, basePath], function() {

        // Logger.addToLog(result.value);
        // Logger.addToLog('');
        // for(var i in result) {
        //     Logger.addToLog(i + " : " + result[i]);
        // }
        browser.waitForElementVisible('#main', 3000000, function() {
            if(callback) {
                callback();
            }
        });
    });
}

/**
 * This function will send all information about coverage from current test
 * HTML report. If your test have some JS scripts coverage checking,
 * make sure to call this function right before closing your browser at the end.
 *
 * @function
 * @param {Object} browser - nightwatch browser
 * @param {Function} callback - will be called after script execute all needed async actions
 */
function reportTestCoverage(browser, callback) {
    browser.execute(function() {
        var coverage_obj_for_nightwatch = {};
        if(window._$blanket) {
            for(var i in window._$blanket) {
                coverage_obj_for_nightwatch[i] = {};
                coverage_obj_for_nightwatch[i].coverage = window._$blanket[i];
                coverage_obj_for_nightwatch[i].source = window._$blanket[i].source;
            }
        }
        return coverage_obj_for_nightwatch;
    },[],function(result) {
        var r = result.value;

        if(!isObjectEmpty(r)) {
            browser.globals.reporter(false,false,{ "coverage": r });
        }

        if(callback) {
            callback();
        }
    });
}

function isObjectEmpty(obj) {
    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            return false;
    }

    return true && JSON.stringify(obj) === JSON.stringify({});
}

module.exports.addFilesToCover = addFilesToCover;
module.exports.reportTestCoverage = reportTestCoverage;
