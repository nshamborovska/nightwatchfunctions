/**  @module includes/helpers */

/**
 * Getting current date and time in readable format
 *
 * @function
 * @author R.Shamborovskyi
 * @returns {String} currentDateTime - dd-mm-yyy_hh-mm
 */
function getCurrentDateTime() {
    var today = new Date();
    var h = today.getHours();
    var i = today.getMinutes();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!

    var yyyy = today.getFullYear();
    if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    }
    if(i<10){
        i='0'+i
    } 
    today = dd+'-'+mm+'-'+yyyy+'_'+h+'-'+i;
    return today;
}

/**
 * This function helps to get element by its attribute value.<br>
 * Element Selenium internal ID is returned in callback param.
 *
 * @function
 * @author  R.Shamborovskyi
 * @param {Object} browser - Nightwatch browser
 * @param {String} selector - CSS selector of searched element in document scope
 * @param {String} attrName - The name of an attribute
 * @param {String} attrValue - The value of an attribute
 * @param {Function} callback - Callback which is called when element found. Internal Selemium ElementID is passed as an argument.
 */
function getElementByAttr(browser, selector, attrName, attrValue, callback) {
	browser
	.elements('css selector', selector, function(elements) {
		for(var i in elements.value) {
			var found = false;
			(function(index) {
				browser.elementIdAttribute(elements.value[index].ELEMENT, attrName, function(attr) {
					if(!found && attr.value == attrValue) {
						if(callback) {
							callback(elements.value[index].ELEMENT);
							found = true;
						}
					}
				});
			})(i);
		}
	});
}

/**
 * This function waits for some text in some element.<br>
 * Element Selenium internal ID is returned in callback param.
 *
 * @function
 * @author  R.Shamborovskyi
 * @param {Object} browser - Nightwatch browser
 * @param {String} selector - CSS selector to search an element
 * @param {String} expectedValue - Expected text
 * @param {Integer} timeout - Maximum time for search (ms). After exceeded, test will fail.
 * @param {Function} callback - Callback which is called when element found or timeout exceeded. Internal Selemium ElementID or false is passed as an argument.
 */
function waitForText(browser, selector, expectedValue, timeout, callback) {

	var found = false;
	var timeStart = new Date().getTime();

	function checkIfPresent() {
		browser.pause(1000)
		.elements('css selector', selector, function(elements) {

			for(var i in elements.value) {
				(function(index) {
					if(found) return;
					browser.elementIdText(elements.value[index].ELEMENT, function(result) {
						if(result.value == expectedValue) {
							found = elements.value[index].ELEMENT;
						}
					});
				})(i);
			}

			var timeNow = new Date().getTime();
			timeout -= (timeNow-timeStart);
			timeStart = timeNow;

			if(!found) {
				if(timeout < 0) {
					callback(found);
				} else {
					checkIfPresent();
				}
			} else {
				console.log("Element with text "+expectedValue+" found in \""+selector+"\"");
				callback(found);
			}
		});
	}

	checkIfPresent();
}

/**
 * This function clicks a button inside dataTable based on given selectors and expected text in one of the cells in desired row.<br>
 * Useful when you have for example a form name, and you want to click edit or delete button having only that name, no ids etc.
 *
 * @function
 * @author  R.Shamborovskyi
 * @param {Object} browser - Nightwatch browser
 * @param {String} rowText - a text to search for in table cells (form name, company name etc)
 * @param {String} tableSelector - selector to the desired &lt;table&gt; element
 * @param {String} buttonSelector - selector to the desired button inside table cell
 */
function clickButtonInTableRow(browser, rowText, tableSelector, buttonSelector) {
	browser.pause(2000);
	browser.execute(function(rowText, tableSelector, buttonSelector) {
		var log = [];
		$(tableSelector+' tbody tr').each(function() {
			var thisRow = false;
			var clicked = false;
			$(this).find('td').each(function() {
				var td_content = $.trim($(this).html());
				if(td_content == rowText) {
					thisRow = true;
					return false;
				}
			});
			if(thisRow && !clicked) {
				var btn = $(this).find(buttonSelector);
				if(btn.length > 0) {
					btn[0].click();
					clicked = true;
					return false;
				}
			}
			if(clicked) {
				return false;
			}
		});
		return log;
	}, [rowText, tableSelector, buttonSelector], function(log) {
		//console.log(log);
	});
}

/**
 * Deletes all cookies for current site.
 *
 * @function
 * @author  R.Shamborovskyi
 * @param {Object} browser - Nightwatch browser
 */
function deleteAllCookies(browser) {
	browser.execute(function() {
		var cookies = document.cookie.split(";");

	    for (var i = 0; i < cookies.length; i++) {
	    	var cookie = cookies[i];
	    	var eqPos = cookie.indexOf("=");
	    	var name = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;
	    	document.cookie = name + "=;expires=Thu, 01 Jan 1970 00:00:00 GMT";
	    }
	});
}

/**
 * Go to left menu item in smartadmin interface
 *
 * Requires:<br>
 * - browser.globals.url - with root url<br>
 * - browser.globals.Helpers - with pointer to <a href="module-includes_helpers.html">Helpers</a> module.
 *
 * @function
 * @author  R.Shamborovskyi
 * @param {Object} browser - Nightwatch browser
 * @param {String} path - URL from left menu item
 */
function goToLeftMenuItem(browser, path) {
	browser
	.waitForElementPresent('#left-panel nav ul li a', browser.globals.defaultTimeout);

	browser.jqueryClick('#left-panel nav ul li a[href="'+browser.globals.url+path+'"]');
	browser.jqueryClick('#left-panel nav ul li a[href="/'+path+'"]');

	// console.log(browser.globals.url+path);
	// // Get companies menu item and click it
	// getElementByAttr(
	// 	browser,
	// 	"#left-panel nav ul li a",
	// 	"href",
	// 	browser.globals.url+path,
	// 	function(elementID) {
	// 		browser.elementIdClick(elementID);
	// 	}
	// );
}

module.exports.getCurrentDateTime = getCurrentDateTime;
module.exports.getElementByAttr = getElementByAttr;
module.exports.waitForText = waitForText;
module.exports.clickButtonInTableRow = clickButtonInTableRow;
module.exports.deleteAllCookies = deleteAllCookies;
module.exports.goToLeftMenuItem = goToLeftMenuItem;


