/**  @module includes/settings */

// FormsCreator settings
var FC_Settings = {
	"URLs" : {
		"local" : "http://localhost:8089/QuestionnaireEngine/",
		"test"  : "https://test-creator.formsengine.io/",
		"acc"   : "https://acc-creator.formsengine.io/",
		"prod"  : "https://creator.formsengine.io/"
	}
	
};

// FormsPublisher settings
var FP_Settings = {
	"URLs" : {
		"local" : "http://localhost:9020/VernePublisher/",
		"test"  : "https://test-publisher.formsengine.io/",
		"acc"   : "https://acc-publisher.formsengine.io/",
		"prod"  : "https://publisher.formsengine.io"
	},
	"publisherName" : {
		"local" : "ATF publisher",
		"test"  : "ATF publisher",
		"acc"   : "ATF publisher",
		"prod"  : "ATF publisher"
	},
	"token" : {
		"local" : "laYZKarq",
		"test"  : "oZBZerrq",
		"acc"   : "oPMZca7q",
		"prod"  : "?"
	},
	"credentials" : {
		"local" : {
			"login" : "admin",
			"password" : "adminL4tUS1n"
		},
		"test" : {
			"login" : "admin",
			"password" : "adminL4tUS1n"
		},
		"acc" : {
			"login" : "admin",
			"password" : "adminL4tUS1n"
		},
		"prod" : {
			"login" : "mycubesadmin",
			"password" : "mycubesadmin"
		}
	}
};


// TestRail settings
var TestRails_settings = {
	"host": "mycubes.testrail.net",
	"login": "Nightwatch.MyCubes@gmail.com",
	"password": "07010327",
	"test_plan_id": 7
};

/**
 * Returns FormsCreator url depending on environment.<br>
 *
 * @function
 * @author  R.Shamborovskyi
 * @param {Object} browser - nightwatch browser
 * @param {String} env - environment
 * @returns {String} - FormsCreator URL for that environment
 */
function getFormsCreatorUrl(browser,env) {
	if(!FC_Settings.URLs[env]) {
		browser.assert.title("ERROR","ERROR: Unknown env");
		browser.end();
	}
	return FC_Settings.URLs[env];
}

/**
 * Returns FormsPublisher url depending on environment.<br>
 *
 * @function
 * @author  R.Shamborovskyi
 * @param {Object} browser - nightwatch browser
 * @param {String} env - environment
 * @returns {String} - FormsPublisher URL for that environment
 */
function getFormsPublisherUrl(browser,env) {
	if(!FP_Settings.URLs[env]) {
		browser.assert.title("ERROR","ERROR: Unknown env");
		browser.end();
	}
	return FP_Settings.URLs[env];
}

/**
 * Returns publisher name depending on environment.<br>
 *
 * @function
 * @author  R.Shamborovskyi
 * @param {Object} browser - nightwatch browser
 * @param {String} env - environment
 * @returns {String} - publisher name for that environment
 */
function getFormsPublisherName(browser,env) {
	if(!FP_Settings.publisherName[env]) {
		browser.assert.title("ERROR","ERROR: Unknown env");
		browser.end();
	}
	return FP_Settings.publisherName[env];
}

/**
 * Returns publisher token depending on environment.<br>
 *
 * @function
 * @author  R.Shamborovskyi
 * @param {Object} browser - nightwatch browser
 * @param {String} env - environment
 * @returns {String} - publisher token for that environment
 */
function getFormsPublisherToken(browser,env) {
	if(!FP_Settings.token[env]) {
		browser.assert.title("ERROR","ERROR: Unknown env");
		browser.end();
	}
	return FP_Settings.token[env];
}

/**
 * Returns publisher credentials depending on environment.<br>
 *
 * @function
 * @author  R.Shamborovskyi
 * @param {Object} browser - nightwatch browser
 * @param {String} env - environment
 * @returns {Object} - JSON with 2 string properties: login, password
 */
function getFormsPublisherCredentials(browser,env) {
	if(!FP_Settings.credentials[env]) {
		browser.assert.title("ERROR","ERROR: Unknown env");
		browser.end();
	}
	return FP_Settings.credentials[env];
}

module.exports.getFormsCreatorUrl = getFormsCreatorUrl;
module.exports.getFormsPublisherUrl = getFormsPublisherUrl;
module.exports.getFormsPublisherName = getFormsPublisherName;
module.exports.getFormsPublisherToken = getFormsPublisherToken;
module.exports.getFormsPublisherCredentials = getFormsPublisherCredentials;
module.exports.TestRailSettings = TestRails_settings;