const https = require('https');

var TR_Settings = require('./settings.js').TestRailSettings;
var testCasesMap = [];

var TR = {

    getTestCases: function(callback) {
        if(testCasesMap.length > 0) {
            // Map is already stored, return in
            callback(null, testCasesMap);
        } else {
            // No map. Need to get it.
            this.call("GET", "get_plan/"+TR_Settings.test_plan_id, null, function(err, response) {
                if(err) {
                    callback(err);
                    return;
                }

                // Get all runs IDs
                var runIDs = [];
                for(var entryN in response.entries) {
                    for(var runN in response.entries[entryN].runs) {
                        runIDs.push(response.entries[entryN].runs[runN].id);
                    }
                }

                // Get tests from runs
                var runsProcessed = 0;
                var error = false;
                for(var i in runIDs) {
                    if(error) {
                        callback(err);
                        return;
                    }
                    TR.call("GET", "get_tests/"+runIDs[i], null, function(err, response) {
                        if(err) {
                            error = true;
                            return;
                        }
                        for(var testIndex in response) {
                            testCasesMap.push(response[testIndex]);
                        }
                        runsProcessed++;
                        if(runsProcessed == runIDs.length) {
                            callback(null, testCasesMap);
                        }
                    });
                }

            });
        }
    },

    getRunIdByTestCase: function(testCaseId, callback) {
        this.getTestCases(function(err, testcases) {
            if(err) {
                callback(err);
                return;
            }
            for(var i in testcases) {
                if(testcases[i].case_id == testCaseId) {
                    callback(null, testcases[i].run_id);
                    return;
                }
            }
            callback("TC not found");
        });
    },

    setStatus: function(statusId, testCaseId, comment, callback) {
        comment = comment || "";
        this.getRunIdByTestCase(testCaseId, function(err, runId) {
            if(err) {
                callback(err);
                return;
            }
            var data = {
                status_id: statusId,
                comment: comment
            };
            TR.call("POST", "add_result_for_case/"+runId+"/"+testCaseId, data, function(err, response) {
                callback(null, response);
            });
        });
    },

    call: function(method, path, data, callback) {

        method = method || "GET";
        path = path || "";
        data = data || {};

        var auth = 'Basic ' + new Buffer(TR_Settings.login + ':' + TR_Settings.password).toString('base64');

        var post_data = JSON.stringify(data);

        var options = {
            host: TR_Settings.host,
            path: "/index.php?/api/v2/"+path,
            method: method,
            headers: {
                'Authorization': auth,
                'Content-Type': 'application/json',
                'Content-Length': Buffer.byteLength(post_data)
            }
        };

        // Set up the request
        var post_req = https.request(options, function(res) {
            res.setEncoding('utf8');
            var body = "";
            res.on('data', function(chunk) {
                body += chunk;
            });
            res.on('end', function() {
                var b = JSON.parse(body);
                callback(null, b);
            });
            res.on('error', function(err) {
                console.error(err.stack);
                callback(err);
            });
        });

        // Post the data
        post_req.write(post_data);
        post_req.end();
    }
};

module.exports = TR;