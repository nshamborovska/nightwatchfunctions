var Question = 'Question';
var Group = 'Group';
var questionOriginalTypes = {
    question: 'question',
    textfield: 'textfield',
    date: 'date',
    hidden: 'hidden',
    textlabel: 'textlabel',
    textarea: 'textarea',
    radio: 'radio',
    checkbox: 'checkbox',
    dropdown: 'dropdown',
    widget: 'widget',
    asyncupload: 'asyncupload',
    asyncmultiupload: 'asyncmultiupload',
    upload: 'upload',
    repeatupload: 'repeatupload',
    page: 'page',
    summary: 'summary'
};

var questionBaseIds = {
    Question: 'Question',
    TextField: 'TextField',
    RegullarExpression_mask: 'RegullarExpression_mask',
    CurrencyAfter_mask: 'CurrencyAfter_mask',
    DateField: 'DateField',
    HiddenField: 'HiddenField',
    TextLabel: 'TextLabel',
    TextArea: 'TextArea',
    RadioButtons: 'RadioButtons',
    CheckBoxes: 'CheckBoxes',
    DropDown: 'DropDown',
    AsyncUploadField: 'AsyncUploadField',
    AsyncMultiUpload: 'AsyncMultiUpload',
    UploadField: 'UploadField',
    RepeatUploadField: 'RepeatUploadField',
    Widget: 'Widget',
    Summary: 'Summary',
    Repeater: 'Repeater',
    Dependency: 'Dependency',
    API_Test: 'API_Test',
    SetAnswer: 'SetAnswer',
    SetAnswer_EmptyAnswer: 'SetAnswer_EmptyAnswer',
    CheckBoxes_Repeater: 'CheckBoxes_Repeater',
    EmailForAnswers: 'EMAIL_FOR_ANSWERS',
    SimpleCondition: 'SimpleCondition',
    AdvancedCondition: 'AdvancedCondition',
    Library: 'Library',
    RepeaterCondition: 'Repeater_condition',
    CheckBoxes_OptionUnderRepeater: 'CheckBoxes_OptionUnderRepeater'
};

var formIds = questionBaseIds;
var Page1Id = 'Page 1';
var Page2Id = 'Page 2';
var Page3Id = 'Page 3';
var Page4Id = 'Page 4';
var Page5Id = 'Page 5';
var Page6Id = 'Page 6';
var Page7Id = 'Page 7';
var Page8Id = 'Page 8';
var Page9Id = 'Page 9';
var Page10Id = 'Page 10';


function QuestionType(type, name, inputType, elementId, rel) {
    this.type = type;
    this.name = name;
    this.inputType = inputType;
    this.elementId = elementId;
    this.rel = rel;
}

function OptionType(name, preSelected, disabled, parent) {
    this.name = name;
    this.preSelected = preSelected;
    this.disabled = disabled;
    this.parent = parent;
}

function FormElement(name, elementId, rel) {
    this.name = name;
    this.elementId = elementId;
    this.rel = rel;
}

function buildId(questionId) {
    return questionId + '_id';
}

function buildPage1Name(questionId) {
    return Page1Id + ' ' + formIds.Question + ' ' + questionId;
}

function buildPage2Name(questionId) {
    return Page2Id + ' ' + formIds.Question + ' ' + questionId;
}

function buildPage3Name(questionId) {
    return Page3Id + ' ' + formIds.Question + ' ' + questionId;
}

function buildPage4Name(questionId) {
    return Page4Id + ' ' + formIds.Question + ' ' + questionId;
}
function buildPage5Name(questionId) {
    return Page5Id + ' ' + formIds.Question + ' ' + questionId;
}
function buildPage6Name(questionId) {
    return Page6Id + ' ' + formIds.Question + ' ' + questionId;
}
function buildPage7Name(questionId) {
    return Page7Id + ' ' + formIds.Question + ' ' + questionId;
}
function buildPage8Name(questionId) {
    return Page8Id + ' ' + formIds.Question + ' ' + questionId;
}
function buildPage9Name(questionId) {
    return Page9Id + ' ' + formIds.Question + ' ' + questionId;
}
function buildPage10Name(questionId) {
    return Page9Id + ' ' + formIds.Question + ' ' + questionId;
}



function buildNameForGroup(elementId) {
    return Group + ' ' + buildPage2Name(elementId);
}

var page1RadioButtons = buildPage1Name(formIds.RadioButtons);
var page1CheckBoxes = buildPage1Name(formIds.CheckBoxes);
var page1DropDown = buildPage1Name(formIds.DropDown);
var page2GroupRadioButtons = buildNameForGroup(formIds.RadioButtons);
var page2GroupCheckBoxes = buildNameForGroup(formIds.CheckBoxes);
var page2GroupDropDown = buildNameForGroup(formIds.DropDown);
var page2CheckBoxesRepeater = buildPage2Name(formIds.CheckBoxes_Repeater);
var page2RadioButtons = buildPage2Name(formIds.RadioButtons);
var page2CheckboxOptionUnderRepeater = buildPage2Name(formIds.CheckBoxes_OptionUnderRepeater);

var page1conditionOptions = buildPage1Name(formIds.page1QuestionsConditionForm);
var page2conditionOptions = buildPage2Name(formIds.page2QuestionsConditionForm);
var page3conditionOptions = buildPage3Name(formIds.page3QuestionsConditionForm);
var page4conditionOptions = buildPage4Name(formIds.page4QuestionsConditionForm);
var page5conditionOptions = buildPage5Name(formIds.page5QuestionsConditionForm);
var page6conditionOptions = buildPage6Name(formIds.page6QuestionsConditionForm);
var page7conditionOptions = buildPage7Name(formIds.page7QuestionsConditionForm);
var page8conditionOptions = buildPage8Name(formIds.page8QuestionsConditionForm);


module.exports = {

    'questionBaseIds': questionBaseIds,

    'questionOriginalTypes': questionOriginalTypes,

    'page1Questions': {
        TextField: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.TextField), questionOriginalTypes.textfield, buildId(formIds.TextField), 2),
        TextField2: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.TextField + '2'), questionOriginalTypes.textfield, buildId(formIds.TextField + '2'), 2),
        TextField3: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.TextField + '3'), questionOriginalTypes.textfield, buildId(formIds.TextField + '3'), 2),
        RegullarExpression_mask: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.RegullarExpression_mask), questionOriginalTypes.textfield, buildId(formIds.RegullarExpression_mask), 2),
        CurrencyAfter_mask: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.CurrencyAfter_mask), questionOriginalTypes.textfield, buildId(formIds.CurrencyAfter_mask), 2),
        DateField: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.DateField), questionOriginalTypes.date, buildId(formIds.DateField), 5),
        DateField2: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.DateField + '2'), questionOriginalTypes.date, buildId(formIds.DateField + '2'), 5),
        HiddenField: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.HiddenField), questionOriginalTypes.hidden, buildId(formIds.HiddenField), 9),
        TextLabel: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.TextLabel), questionOriginalTypes.textlabel, buildId(formIds.TextLabel), 4),
        TextArea: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.TextArea), questionOriginalTypes.textarea, buildId(formIds.TextArea), 3),
        RadioButtons: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.RadioButtons), questionOriginalTypes.radio, buildId(formIds.RadioButtons), 6),
        CheckBoxes: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.CheckBoxes), questionOriginalTypes.checkbox, buildId(formIds.CheckBoxes), 7),
        DropDown: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.DropDown), questionOriginalTypes.dropdown, buildId(formIds.DropDown), 8),
        Widget: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.Widget), questionOriginalTypes.widget, buildId(formIds.Widget), 14),
        TextFieldDependency1: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.Dependency + '1'), questionOriginalTypes.textfield, buildId(formIds.Dependency + '1'), 2),//TextField + '_Dependency1'
        TextFieldDependency2: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.Dependency + '2'), questionOriginalTypes.textfield, buildId(formIds.Dependency + '2'), 2),//TextField + '_Dependency2'
        TextLabelDependency3: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.Dependency + '3'), questionOriginalTypes.textlabel, buildId(formIds.Dependency + '3'), 4),//TextLabel + '_Dependency3'
        AsyncUploadField: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.AsyncUploadField), questionOriginalTypes.asyncupload, buildId(formIds.AsyncUploadField), 16),
        AsyncMultiUpload: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.AsyncMultiUpload), questionOriginalTypes.asyncmultiupload, buildId(formIds.AsyncMultiUpload), 17),
        UploadField: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.UploadField), questionOriginalTypes.upload, buildId(formIds.UploadField), 10),
        RepeatUploadField: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.RepeatUploadField), questionOriginalTypes.repeatupload, buildId(formIds.RepeatUploadField), 15),
        //EMAIL_FOR_ANSWERS: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.EmailForAnswers), questionOriginalTypes.textfield, buildId(formIds.EmailForAnswers), 2)
        SetAnswer: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.SetAnswer), questionOriginalTypes.textfield, buildId(formIds.SetAnswer), 2),
        API_Test: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.API_Test), questionOriginalTypes.widget, buildId(formIds.API_Test), 14),
        SetAnswer_EmptyAnswer: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.SetAnswer_EmptyAnswer), questionOriginalTypes.textfield, buildId(formIds.SetAnswer_EmptyAnswer), 2),
        TextField4: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.TextField + '4'), questionOriginalTypes.textfield, buildId(formIds.TextField + '4'), 2),
        Widget2: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.Widget + '2'), questionOriginalTypes.widget, buildId(formIds.Widget + '2'), 14)

    },

    'page1Options': {
        RadioButtonsOption1: new OptionType('RadiobuttonOptionPreselected', true, false, page1RadioButtons),
        RadioButtonsOption2: new OptionType('RadiobuttonOptionDisabled', false, true, page1RadioButtons),
        RadioButtonsOption3: new OptionType('RadiobuttonOptionDefault', false, false, page1RadioButtons),
        RadioButtonsOption4: new OptionType('RadiobuttonOptionDefault4', false, false, page1RadioButtons),
        RadioButtonsOption5: new OptionType('RadiobuttonOptionDefault5', false, false, page1RadioButtons),
        RadioButtonsOption6: new OptionType('RadiobuttonOptionDefault6', false, false, page1RadioButtons),
        CheckboxOption1: new OptionType('CheckboxPreselected', true, false, page1CheckBoxes),
        CheckboxOption2: new OptionType('CheckboxDisabled', false, true, page1CheckBoxes),
        CheckboxOption3: new OptionType('CheckboxDefault', false, false, page1CheckBoxes),
        CheckboxOption4: new OptionType('CheckboxDefault4', false, false, page1CheckBoxes),
        CheckboxOption5: new OptionType('CheckboxDefault5', false, false, page1CheckBoxes),
        CheckboxOption6: new OptionType('CheckboxDefault6', false, false, page1CheckBoxes),
        CheckboxOption7: new OptionType('Checkbox Default 7', false, false, page1CheckBoxes),
        DropDownOption1: new OptionType('DropDownPreselected', true, false, page1DropDown),
        DropDownOption2: new OptionType('DropDownDisabled', false, true, page1DropDown),
        DropDownOption3: new OptionType('DropDownDefault', false, false, page1DropDown),
        DropDownOption4: new OptionType('DropDownDefault4', false, false, page1DropDown)
    },

    'page2Elements': {
        TextField: new QuestionType(questionOriginalTypes.question, buildPage2Name(formIds.TextField), questionOriginalTypes.textfield, buildId(formIds.TextField), 2),
        SimpleCondition: new QuestionType(questionOriginalTypes.question, buildPage2Name(formIds.SimpleCondition), formIds.SimpleCondition, buildId(formIds.SimpleCondition), 20),
        AdvancedCondition: new QuestionType(questionOriginalTypes.question, buildPage2Name(formIds.AdvancedCondition), formIds.AdvancedCondition, buildId(formIds.AdvancedCondition), 20),
        RadioButtons: new QuestionType(questionOriginalTypes.question, buildPage2Name(formIds.RadioButtons), questionOriginalTypes.radio, buildId(formIds.RadioButtons + '2'), 6),
        Summary: new QuestionType(questionOriginalTypes.question, buildPage2Name(formIds.Summary), questionOriginalTypes.summary, buildId(formIds.Summary), 12),
        EmailForAnswers: new QuestionType(questionOriginalTypes.question, formIds.EmailForAnswers, questionOriginalTypes.textfield, buildId(formIds.EmailForAnswers), 2),
        Group: new FormElement(buildPage2Name(Group), buildId(Group), '18'),
        Repeater: new FormElement(buildPage2Name(formIds.Repeater), buildId(formIds.Repeater), '21'),
        AsyncUploadField: new QuestionType(questionOriginalTypes.question, buildPage2Name(formIds.AsyncUploadField), questionOriginalTypes.asyncupload, buildId(formIds.AsyncUploadField), 16),
        CreateLibrary: new FormElement(buildPage2Name(formIds.Library), buildId(formIds.Library), '22'),
        Library: new FormElement(buildPage2Name(formIds.Library), buildId(formIds.Library), '19'),
        TextFieldForLibrary: new QuestionType(questionOriginalTypes.question, buildPage2Name(formIds.TextField + ' for Library'), questionOriginalTypes.textfield, buildId(formIds.TextField + '_' + formIds.Library), 2),
        TextFieldForRepeater: new QuestionType(questionOriginalTypes.question, buildPage2Name(formIds.TextField + ' for Repeater'), questionOriginalTypes.textfield, buildId(formIds.TextField), 2), //FIXME id
        TextFieldForSimpleCondition: new QuestionType(questionOriginalTypes.question, buildPage2Name(formIds.TextField + ' for SimpleCondition'), questionOriginalTypes.textfield, buildId(formIds.TextField), 2), //FIXME id
        TextFieldForAdvancedCondition: new QuestionType(questionOriginalTypes.question, buildPage2Name(formIds.TextField + ' for AdvancedCondition'), questionOriginalTypes.textfield, buildId(formIds.TextField), 2), //FIXME id
        CheckBoxes_Repeater: new QuestionType(questionOriginalTypes.question, buildPage2Name(formIds.CheckBoxes_Repeater), questionOriginalTypes.checkbox, buildId(formIds.CheckBoxes_Repeater), 7),
        CheckBoxes_OptionUnderRepeater: new QuestionType(questionOriginalTypes.question, formIds.CheckBoxes_OptionUnderRepeater, questionOriginalTypes.checkbox, buildId(formIds.CheckBoxes_OptionUnderRepeater), 7)
    },

    'page2Options': {
        RadioButtonsOption1: new OptionType('RadiobuttonOptionPreselected', true, false, page2RadioButtons),
        RadioButtonsOption2: new OptionType('RadiobuttonOptionDisabled', false, true, page2RadioButtons),
        RadioButtonsOption3: new OptionType('RadiobuttonOptionDefault', false, false, page2RadioButtons),
        CheckboxOption_Repeater1: new OptionType('CheckboxDefault_Repeater1', false, false, page2CheckBoxesRepeater),
        CheckboxOption_Repeater2: new OptionType('CheckboxDefault_Repeater2', false, false, page2CheckBoxesRepeater),
        CheckboxOption_Repeater3: new OptionType('CheckboxDefault_Repeater3', false, false, page2CheckBoxesRepeater),
    },

    'page2CheckboxOptionUnderRepeater_Option1': {
        CheckboxOptionUnderRepeater_Option1: new OptionType('CheckboxDefault_Option1', false, false, page2CheckboxOptionUnderRepeater)
    },

    'page2GroupQuestions': {
        TextField: new QuestionType(questionOriginalTypes.question, buildNameForGroup(formIds.TextField), questionOriginalTypes.textfield, buildId(formIds.TextField), 2),
        Widget: new QuestionType(questionOriginalTypes.question, buildNameForGroup(formIds.Widget), questionOriginalTypes.widget, buildId(formIds.Widget), 14),
        DateField: new QuestionType(questionOriginalTypes.question, buildNameForGroup(formIds.DateField), questionOriginalTypes.date, buildId(formIds.DateField), 5),
        HiddenField: new QuestionType(questionOriginalTypes.question, buildNameForGroup(formIds.HiddenField), questionOriginalTypes.hidden, buildId(formIds.HiddenField), 9),
        TextLabel: new QuestionType(questionOriginalTypes.question, buildNameForGroup(formIds.TextLabel), questionOriginalTypes.textlabel, buildId(formIds.TextLabel), 4),
        TextArea: new QuestionType(questionOriginalTypes.question, buildNameForGroup(formIds.TextArea), questionOriginalTypes.textarea, buildId(formIds.TextArea), 3),
        RadioButtons: new QuestionType(questionOriginalTypes.question, buildNameForGroup(formIds.RadioButtons), questionOriginalTypes.radio, buildId(formIds.RadioButtons + '2'), 6),
        CheckBoxes: new QuestionType(questionOriginalTypes.question, buildNameForGroup(formIds.CheckBoxes), questionOriginalTypes.checkbox, buildId(formIds.CheckBoxes + '2'), 7),
        DropDown: new QuestionType(questionOriginalTypes.question, buildNameForGroup(formIds.DropDown), questionOriginalTypes.dropdown, buildId(formIds.DropDown + '2'), 8),
        AsyncUploadField: new QuestionType(questionOriginalTypes.question, buildNameForGroup(formIds.AsyncUploadField), questionOriginalTypes.asyncupload, buildId(formIds.AsyncUploadField), 16),
        UploadField: new QuestionType(questionOriginalTypes.question, buildNameForGroup(formIds.UploadField), questionOriginalTypes.upload, buildId(formIds.UploadField), 10),
        RepeatUploadField: new QuestionType(questionOriginalTypes.question, buildNameForGroup(formIds.RepeatUploadField), questionOriginalTypes.repeatupload, buildId(formIds.RepeatUploadField), 15)
    },

    'page2GroupOptions': {
        DropDownOption1: new OptionType('1', false, false, page2GroupDropDown),
        DropDownOption2: new OptionType('2', false, false, page2GroupDropDown),
        DropDownOption3: new OptionType('3', false, false, page2GroupDropDown),
        DropDownOption4: new OptionType('4', false, false, page2GroupDropDown),
        DropDownOption5: new OptionType('5', false, false, page2GroupDropDown),
        RadioButtonsOption1: new OptionType('RadiobuttonOptionDefault5', false, false, page2GroupRadioButtons),
        RadioButtonsOption2: new OptionType('RadiobuttonOptionDefault6', false, false, page2GroupRadioButtons),
        CheckboxOption1: new OptionType('CheckboxDefault5', false, false, page2GroupCheckBoxes),
        CheckboxOption2: new OptionType('CheckboxDefault6', false, false, page2GroupCheckBoxes),
        CheckboxOption3: new OptionType('Checkbox Default 7', false, false, page2GroupCheckBoxes)
    },

    'page2GroupOtherElements': {
        Repeater_condition: new FormElement(buildNameForGroup(formIds.RepeaterCondition), buildId(formIds.RepeaterCondition), '21'),
        TextField: new QuestionType(questionOriginalTypes.question, buildNameForGroup('Group Repeater ' + formIds.TextField), questionOriginalTypes.textfield, buildId(formIds.TextField), 2) //FIXME id
    },

    'page3Elements': {
        TextField: new QuestionType(questionOriginalTypes.question, buildPage3Name(formIds.TextField), questionOriginalTypes.textfield, buildId(formIds.TextField), 2) //FIXME id
    },

    'page1QuestionsConditionForm': {
        TextField: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.TextField + 'Condition1'), questionOriginalTypes.textfield, buildId(formIds.TextField + 'Condition1'), 2),
        TextField2: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.TextField +'2'), questionOriginalTypes.textfield, buildId(formIds.TextField + 'Condition01'), 2),
        TextField3: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.TextField + '3'), questionOriginalTypes.textfield, buildId(formIds.TextField + 'Condition02'), 2),
        TextField4: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.TextField + '4'), questionOriginalTypes.textfield, buildId(formIds.TextField + 'Condition03'), 2),
        TextField5: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.TextField + '5'), questionOriginalTypes.textfield, buildId(formIds.TextField + 'Condition04'), 2),

        SimpleCondition: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.SimpleCondition), formIds.SimpleCondition, buildId(formIds.SimpleCondition), 20),
        AdvancedCondition: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.AdvancedCondition), formIds.AdvancedCondition, buildId(formIds.AdvancedCondition), 20),
        TextFieldForSimpleCondition: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.TextField + ' for SimpleCondition'), questionOriginalTypes.textfield, buildId(formIds.TextField), 2), //FIXME id
        TextFieldForAdvancedCondition: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.TextField + ' for AdvancedCondition'), questionOriginalTypes.textfield, buildId(formIds.TextField), 2), //FIXME id

        RadioButtons: new QuestionType(questionOriginalTypes.question, buildPage1Name(formIds.RadioButtons), questionOriginalTypes.radio, buildId(formIds.RadioButtons), 6),
        RadioButtonsOption1: new OptionType('RadiobuttonOptionPreselected', true, false, page1conditionOptions),
        RadioButtonsOption2: new OptionType('RadiobuttonOptionDisabled', false, true, page1conditionOptions),
        RadioButtonsOption3: new OptionType('RadiobuttonOptionDefault', false, false, page1conditionOptions)

    },

    'page2QuestionsConditionForm': {
        TextField: new QuestionType(questionOriginalTypes.question, buildPage2Name(formIds.TextField), questionOriginalTypes.textfield, buildId(formIds.TextField + 'Condition2'), 2),
        SimpleCondition: new QuestionType(questionOriginalTypes.question, buildPage2Name(formIds.SimpleCondition), formIds.SimpleCondition, buildId(formIds.SimpleCondition), 20),
        AdvancedCondition: new QuestionType(questionOriginalTypes.question, buildPage2Name(formIds.AdvancedCondition), formIds.AdvancedCondition, buildId(formIds.AdvancedCondition), 20),

        TextFieldForSimpleCondition: new QuestionType(questionOriginalTypes.question, buildPage2Name(formIds.TextField + ' for SimpleCondition'), questionOriginalTypes.textfield, buildId(formIds.TextField), 2), //FIXME id
        TextFieldForAdvancedCondition: new QuestionType(questionOriginalTypes.question, buildPage2Name(formIds.TextField + ' for AdvancedCondition'), questionOriginalTypes.textfield, buildId(formIds.TextField), 2), //FIXME id

        RadioButtons: new QuestionType(questionOriginalTypes.question, buildPage2Name(formIds.RadioButtons), questionOriginalTypes.radio, buildId(formIds.RadioButtons), 6),
        RadioButtonsOption1: new OptionType('RadiobuttonOptionPreselected', true, false, page2conditionOptions),
        RadioButtonsOption2: new OptionType('RadiobuttonOptionDisabled', false, true, page2conditionOptions),
        RadioButtonsOption3: new OptionType('RadiobuttonOptionDefault', false, false, page2conditionOptions)
    },

    'page3QuestionsConditionForm': {
        TextField: new QuestionType(questionOriginalTypes.question, buildPage3Name(formIds.TextField), questionOriginalTypes.textfield, buildId(formIds.TextField + 'Condition3'), 2),
        RadioButtons: new QuestionType(questionOriginalTypes.question, buildPage3Name(formIds.RadioButtons), questionOriginalTypes.radio, buildId(formIds.RadioButtons), 6),
        RadioButtonsOption1: new OptionType('RadiobuttonOptionPreselected', true, false, page3conditionOptions),
        RadioButtonsOption2: new OptionType('RadiobuttonOptionDisabled', false, true, page3conditionOptions),
        RadioButtonsOption3: new OptionType('RadiobuttonOptionDefault', false, false, page3conditionOptions)
    },

    'page4QuestionsConditionForm': {
        TextField: new QuestionType(questionOriginalTypes.question, buildPage4Name(formIds.TextField), questionOriginalTypes.textfield, buildId(formIds.TextField + 'Condition4'), 2),
        RadioButtons: new QuestionType(questionOriginalTypes.question, buildPage4Name(formIds.RadioButtons), questionOriginalTypes.radio, buildId(formIds.RadioButtons), 6),
        RadioButtonsOption1: new OptionType('RadiobuttonOptionPreselected', true, false, page4conditionOptions),
        RadioButtonsOption2: new OptionType('RadiobuttonOptionDisabled', false, true, page4conditionOptions),
        RadioButtonsOption3: new OptionType('RadiobuttonOptionDefault', false, false, page4conditionOptions)
    },

    'page5QuestionsConditionForm': {
        TextField: new QuestionType(questionOriginalTypes.question, buildPage5Name(formIds.TextField), questionOriginalTypes.textfield, buildId(formIds.TextField + 'Condition5'), 2),
        TextField2: new QuestionType(questionOriginalTypes.question, buildPage5Name(formIds.TextField +'2'), questionOriginalTypes.textfield, buildId(formIds.TextField + 'Condition01'), 2),
        TextField3: new QuestionType(questionOriginalTypes.question, buildPage5Name(formIds.TextField + '3'), questionOriginalTypes.textfield, buildId(formIds.TextField + 'Condition02'), 2),
        TextField4: new QuestionType(questionOriginalTypes.question, buildPage5Name(formIds.TextField + '4'), questionOriginalTypes.textfield, buildId(formIds.TextField + 'Condition03'), 2),
        TextField5: new QuestionType(questionOriginalTypes.question, buildPage5Name(formIds.TextField + '5'), questionOriginalTypes.textfield, buildId(formIds.TextField + 'Condition04'), 2),

        RadioButtons: new QuestionType(questionOriginalTypes.question, buildPage5Name(formIds.RadioButtons), questionOriginalTypes.radio, buildId(formIds.RadioButtons), 6),
        RadioButtonsOption1: new OptionType('RadiobuttonOptionPreselected', true, false, page5conditionOptions),
        RadioButtonsOption2: new OptionType('RadiobuttonOptionDefault', false, false, page5conditionOptions)
    }
};