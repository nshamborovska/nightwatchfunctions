/**  @module includes/treeHelpers */

/**
 * Checks if node with given name and type exists in form tree.<br>
 * Browser must be on form tree page.
 *
 * @function
 * @author  R.Shamborovskyi
 * @param {Object} browser - Nightwatch browser
 * @param {String} nodeName - node name
 * @param {String} nodeType - node type (can be page, question etc). If you will pass false here, node type will not matter.
 * @param {String} returnProperty - what property of node to return (can be text, id etc)
 * @param {Function} callback - callback will be called with property value if found, or false if not found
 */
function findTreeNodeByName(browser, nodeName, nodeType, returnProperty, callback) {

	returnProperty = returnProperty || "text";

	browser.execute(function(nodeName,nodeType,returnProperty) {

		function searchInChildren(node,name,type) {
			if(node.children.length === 0) {
				return false;
			}
			for(var i in node.children) {

				var currNode = node.children[i];
				var res = searchInChildren(currNode,name,type);

				if(res) {
					return res;
				}

				var txt = "";
				if(currNode.text) {
					txt = currNode.text;
					var index = txt.indexOf(" ");
					txt = txt.substring(index+1,txt.length);
				}

				if(txt == name) {
					if(type && currNode.type != type) {
						continue;
					}
					if(returnProperty == "text") {
						return txt;
					}
					return currNode[returnProperty];
				}
			}
			return false;
		}

		var root = treeGlobals.treeData[0];
		return searchInChildren(root,nodeName,nodeType);
	},[nodeName,nodeType,returnProperty],function(result) {
		callback(result.value);
	});
}

/**
 * Expands all tree nodes, so they all are visible and ready to be tested, waitForVisible, clicked etc.<br>
 * Browser must be on form tree page.
 *
 * @function
 * @author  R.Shamborovskyi
 * @param {Object} browser - Nightwatch browser
 */
function expandAllNodes(browser) {
	browser.execute(function() {
		$.tree_container.jstree('open_all');
	});
}

/**
 * Gets context menu item of node in form tree (search for node by given name and type) and clicks menu item based on number in "rel" HTML attribute of item.<br>
 * Includes expanding all tree nodes action.
 * Browser must be on form tree page<br>
 *
 * @function
 * @author  R.Shamborovskyi
 * @param {Object} browser - Nightwatch browser
 * @param {String} nodeName - name of node to search
 * @param {String} nodeType - type of node to search. If type is questionnaire, than name doesn't matter (because it can be only 1 questionnaire type node - root node)
 * @param {Integer} itemN - value of "rel" attribute of desired contextmenu item
 */
function clickContextMenuItem(browser, nodeName, nodeType, itemN) {
	// Expand all nodes
	browser.globals.TreeHelpers.expandAllNodes(browser);
	browser.pause(1500);

	if(nodeType == "questionnaire") {
		// browser
		// .moveToElement('#tree_view > ul > li > a',10,10)
		// .mouseButtonClick(2);

		browser.jqueryClick('#tree_view > ul > li > a',true);
	}
	else {
		// Find parent node
		browser.globals.TreeHelpers.findTreeNodeByName(browser, nodeName, nodeType, "id", function(parentNodeId) {
			browser
			// Get context menu
			.execute(function(parentNodeId) {
				$('#'+parentNodeId+' > a').trigger("contextmenu");
			},[parentNodeId]);
		});
	}
	// Wait for context menu and click on item
	browser
	.waitForElementVisible('ul.jstree-contextmenu li a',10000)
	.jqueryClick('ul.jstree-contextmenu li a[rel='+itemN+']');
}

/**
 * Adds form to global structure.
 *
 * @function
 * @author  R.Shamborovskyi
 * @param {Object} browser          - nightwatch browser
 * @param {String} formName         - form name
 * @param {Object} properties       - form properties (plain object) that will require verifying later
 */
function addFormToGlobalStructure(browser, formName, properties) {
	// Define structure global if needed
	if(!browser.globals.structures) {
		browser.globals.structures = {};
	}

	// Add form to structure global
	browser.globals.structures[formName] = {};
	browser.globals.structures[formName].name = formName;

	// Add form properties
	for(var i in properties) {
		browser.globals.structures[formName][i] = properties[i];
	}
}

/**
 * Adds object to global structure.
 *
 * @function
 * @author  R.Shamborovskyi
 * @param {Object} browser          - nightwatch browser
 * @param {String} formName         - form name
 * @param {String} parentName       - name of parent node
 * @param {Object} obj              - plain node object with all properties that will require verifying later
 * @returns {Boolean}               - true if added, false if not found
 */
function addNodeToGlobalStructure(browser, formName, parentName, obj) {
	if(!browser.globals.structures || !browser.globals.structures[formName]) {
		browser.assert.title("ERROR","ERROR: Tried to add node to global structure to undefined form");
		browser.end();
	}

	function searchInChildren(node, wantedName) {
		if(node.name == wantedName) {
			return node;
		}

		var foundNode = false;

		if(node.children) {
			for(var i in node.children) {
				foundNode = searchInChildren(node.children[i], wantedName);
				if(foundNode) { break; }
			}
		}

		if(foundNode) {
			return foundNode;
		}

		return false;
	}

	var form = browser.globals.structures[formName];
	var parentNode = searchInChildren(form, parentName);

	if(parentNode) {
		if(!parentNode.children) {
			parentNode.children = [];
		}

		parentNode.children.push(obj);
		return true;
	}
	return false;
}

/**
 * Compares test structure with real structure from publisher.
 *
 * @function
 * @author R.Shamborovskyi
 * @param {Object} browser          - nightwatch browser
 * @param {String} formName         - form name
 * @param {String} realStructure    - JSON structure from publisher
 * @returns {Boolean}               - true if equal, false if something wrong
 */
function compareTestVsRealStructure(browser, formName, structure) {
	if(!browser.globals.structures[formName]) {
		browser.assert.title("ERROR","ERROR: compareTestVsRealStructure - no test structure");
		browser.end();
	}

	function assertProperty(name, test, real) {
		if(test != real) {
			browser.assert.title("ERROR","ERROR: "+name+" is not equal ("+test+" VS "+real+")");
			browser.end();
		} else {
			console.log(name + " is \""+test+"\" = OK");
		}
	}

	function checkChildrenQuestions(realNode, testNode) {
		for(var i in realNode.questions) {
			var currentRealQuestion = realNode.questions[i];
			var currentTestQuestion = null;

			if(testNode.children && testNode.children.length > 0) {
				for(var j in testNode.children) {
					if(testNode.children[j].name == currentRealQuestion.name) {
						currentTestQuestion = testNode.children[j];
						break;
					}
				}

				assertProperty("form.question.name", currentRealQuestion.name, currentTestQuestion.name);
				assertProperty("form.question.type", currentRealQuestion.type, currentTestQuestion.type);

				if(currentTestQuestion.elementId) {
					assertProperty("form.question.elementId", currentRealQuestion.elementId, currentTestQuestion.elementId);
				}
			}
		}
	}

	var testStructure = browser.globals.structures[formName];
	var realStructure = JSON.parse(structure).structure;

	// Form
	assertProperty("form.name", testStructure.name, realStructure.name);
	assertProperty("form.company", testStructure.company, realStructure.company);
	assertProperty("form.type", testStructure.type, realStructure.type);
	assertProperty("number of pages", testStructure.children.length, realStructure.pages.length);

	// Pages
	for(var i in realStructure.pages) {
		var currentRealPage = realStructure.pages[i];
		var currentTestPage = null;

		for(var j in testStructure.children) {
			if(testStructure.children[j].name == currentRealPage.name) {
				currentTestPage = testStructure.children[j];
				break;
			}
		}

		assertProperty("form.page["+i+"].name", currentRealPage.name, currentTestPage.name);
		assertProperty("form.page["+i+"].type", currentRealPage.type, currentTestPage.type);

		// Questions
		checkChildrenQuestions(currentRealPage, currentTestPage);
	}
}

module.exports.findTreeNodeByName = findTreeNodeByName;
module.exports.expandAllNodes = expandAllNodes;
module.exports.clickContextMenuItem = clickContextMenuItem;
module.exports.addFormToGlobalStructure = addFormToGlobalStructure;
module.exports.addNodeToGlobalStructure = addNodeToGlobalStructure;
module.exports.compareTestVsRealStructure = compareTestVsRealStructure;