var Helpers = require("../includes/helpers.js");
var HtmlReporter = require('nightwatch-html-reporter');

var reporter = new HtmlReporter({
    openBrowser: true,
    reportsDirectory: './reports',
    reportFilename: 'report_'+Helpers.getCurrentDateTime()+'.html',
    themeName: 'mycubes'
});

module.exports = {
    reporter: reporter.fn,
    loadingTimeout: 25000,
    defaultTimeout: 10000,
    shortTimeout: 5000,
    quickTimeout: 1000,
    retryAssertionTimeout: 500
};