/** @module FE/FC/testcases/error_messages */

// Modules
var Helpers = require('./../../includes/helpers.js');

module.exports = {

    /**
     * <b>TESTCASE</b><br>
     * Creates a new error messages.
     *
     * Requires:<br>
     * - browser.globals.url - with root url<br>
     * - browser.globals.Helpers - with pointer to <a href="module-includes_helpers.html">Helpers</a> module.
     *
     * @param {Object} browser          - nightwatch browser
     * @param {String} [errorName]      - error message name. Default: ATF_ErrorMessage
     * @param {String} [errorContent]   - Groovy Script. Default: Dit veld is verplicht
     */

     "Create error message": function (browser, errorName, errorContent) {
          errorName = errorName || "ATF_ErrorMessage";
          errorContent = errorContent || "Dit veld is verplicht";

          // Go to list with error messages
          browser.globals.Helpers.goToLeftMenuItem(browser, "list/messages");

          browser
          // Wait for error messages list html loaded
          .waitForElementPresent("#btn_add_message", browser.globals.defaultTimeout)
          // Click "Add message" button
          .click("#btn_add_message")
          // Wait for dialog visible
          .waitForElementVisible("#dialog_add_message", browser.globals.defaultTimeout)
          // Set name of the error message
          .setValue("#dialog_add_message_name", errorName)
          // Set content of the error message
          .setValue("#dialog_add_message_content", errorContent)

          // Click "Save" button (submit) when it will be visible
          .waitForElementVisible(".modal-footer .btn-submit", browser.globals.defaultTimeout)
          .click(".modal-footer .btn-submit", function () {
               console.log("Dialog submitted");
          });

          var selector = "#dt_message tbody tr td:nth-child(1)";
          browser.globals.Helpers.waitForText(browser, selector, errorName, browser.globals.defaultTimeout, function (elementID) {
            if (!elementID) {
                browser.assert.title("ERROR", "ERROR: Timeout when waiting for text " + errorName + " in \"" + selector + "\"");
                browser.end();
            }
        });

        return errorName;
    },

    /**
     * <b>TESTCASE</b><br>
     * Find error message in a Table<br>
     * Check if error message with name errorName is present in a list<br>
     *
     * @param {Object} browser          - nightwatch browser
     * @param {String} [errorName]      - name of error message which will be searching
     * @param {requestCallback} [true/false, errorName] - The callback that returns: 
     *                                  1) true(if error message is present) or false(if error message is not present)
     *                                  2) errorName wich we can use for message or other actions
     */

    "Find error message in table": function (browser, errorName, callback) {
          // Go to list with error messages
          browser.globals.Helpers.goToLeftMenuItem(browser, "list/messages");

          browser
          .waitForElementVisible("#dt_message", browser.globals.defaultTimeout)
          .execute(function(errorName) {
            var result = false;
            $("#dt_message tbody tr").each(function() {
                var td = $(this).find("td:nth-child(1)");
                if($.trim(td.text()) == errorName) {
                    result = true;
                    return false;
                } else {
                    result = false;
                };
            });
            return result;
          },[errorName], function(result) {
            callback(result.value, errorName);
          });
     },

    /**
     * <b>TESTCASE</b><br>
     * Find error message in table or create it<br>
     * Check if error message with name errorName is present in a list<br>
     * If error message is present, the message in report will say about it.<br>
     * If error message not present, the test will create it.<br>
     *
     * @param {Object} browser          - nightwatch browser
     * @param {String} [errorName]      - name of error message which will be searching/create
     * @param {String} [errorContent]   - content of error message which will be searching/create
     */

    "Find error message in table or create it": function (browser, errorName, errorContent){
        var t = this;
        t["Find error message in table"](browser, errorName, function(result, errorName, errorContent){
            if(result == true){
                browser
                .assert.ok(true, "Error message with name "+errorName+" present on FC");
            }else{
                browser
                .assert.ok(true, "Error message with name "+errorName+" not present on FC");
                t["Create error message"](browser, errorName, errorContent);
            }
        });
    },

    "Delete error message if it already exists" : function (browser, errorName) {
      // Go to list with error messages
      browser.globals.Helpers.goToLeftMenuItem(browser, "list/messages");

      // Delete the error message when "Error messages" tab already opened
        var t = this;
        t["Find error message in table"](browser, errorName, function(result, errorName) {
            if(result == true){
                t["Delete error message"](browser, errorName);
                browser.assert.ok(true, 'Message with name: '+ errorName +' was present and remove');
            } else {
                browser.assert.ok(true, 'Message with name: '+ errorName +' is not present')
            };
        });
    },

    "Delete error message": function (browser, errorName) {
      errorName = errorName || "ATF_ErrorMessage";

      // Go to list with error messages
      browser.globals.Helpers.goToLeftMenuItem(browser, "list/messages");

      browser
      // Wait for error messages table appear
      .waitForElementVisible("#dt_message tbody", browser.globals.defaultTimeout)
      .waitForElementVisible("#dt_message a.btn-danger", browser.globals.defaultTimeout)
      // Click on delete error message button
      browser.globals.Helpers.clickButtonInTableRow(
          browser,
          errorName,
          '#dt_message',
          'a.btn-danger'
        );

        browser
        // Wait for confirmation dialog
        .waitForElementVisible("#Msg1", browser.globals.defaultTimeout)
        .waitForElementVisible("#bot2-Msg1", browser.globals.defaultTimeout)
        // Click on Yes
        .click("#bot2-Msg1")
        .waitForElementVisible(".MessageBoxButtonSection", browser.globals.defaultTimeout)
        // Wait till table will appear again
        .waitForElementVisible("#dt_message tbody", browser.globals.defaultTimeout)

        // Verify that error message is not in the list anymore
        browser.globals.Helpers.waitForText(browser, "#dt_message tbody tr td:nth-child(1)", errorName, browser.globals.defaultTimeout, function(elementID) {
            if(elementID) {
                browser.assert.title("ERROR","ERROR: Error message " + errorName + " is still in the list after deleting");
                browser.end();
            } else {
                console.log("Verified: Error message " + errorName + " is not in the list anymore");
            }
        });
    },

    "Check if error message is not in the list anymore": function (browser, errorName) {
        var t = this;
        t["Find error message in table"](browser, errorName, function(result, errorName) {
            if(result == false){
                browser
                .assert.ok(true, "Error message is not in the list anymore");
            }else if(result == true) {
                browser
                .assert.ok(false, "Error message was\'t deleted, it is still in the list");
            }else{
                browser
                .assert.ok(false, "Something wrong with test");
            }
        });
    }
};

