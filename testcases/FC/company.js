/** @module FE/FC/testcases/company */

var CLI = require('./../../includes/CLI.js');
var Helpers = require('./../../includes/helpers.js');

module.exports = {

	/**
	 * <b>TESTCASE</b><br>
	 * Creates a company.<br>
	 * Verifies that new company was successfully created and present on companies list.<br><br>
	 *
	 * Requires:<br>
	 * - browser.globals.url - with root url<br>
	 * - browser.globals.Helpers - with pointer to <a href="module-includes_helpers.html">Helpers</a> module.
	 *
	 * @param {Object} browser          - nightwatch browser
	 * @param {String} [companyName]    - name of new company. Default: ATF_Company
	 */
	'Create company' : function(browser, companyName) {

		companyName = companyName || "ATF_Company";

		// Go to companies
		browser.globals.Helpers.goToLeftMenuItem(browser, 'list/companies');

		// TODO unhappy flows

		browser
		// Wait for company list html loaded
		.waitForElementPresent('#btn_add_company', browser.globals.defaultTimeout)
		.waitForElementVisible('#btn_add_company', browser.globals.defaultTimeout)
		// Click on add company button
		.click('#btn_add_company')
		// Wait for dialog is open
		.waitForElementPresent('#dialog_edit_company_name', browser.globals.defaultTimeout)
		.waitForElementVisible('#dialog_edit_company_name', browser.globals.defaultTimeout)
		// Set company name value to input
		.setValue('#dialog_edit_company_name',companyName)
		// Click on submit
		.waitForElementPresent('#dialog_edit_company .modal-footer button.btn-success', browser.globals.defaultTimeout)
		.waitForElementVisible('#dialog_edit_company .modal-footer button.btn-success', browser.globals.defaultTimeout)
		.click('#dialog_edit_company .modal-footer button.btn-success', function() {
			console.log("Company dialog submit was clicked");
		});

		// Verify that company appeared in list
		var selector = '#dt_company tbody tr td:first-child';
		browser.globals.Helpers.waitForText(browser, selector, companyName, browser.globals.loadingTimeout, function(elementID) {
			if(!elementID) {
				browser.assert.title("ERROR","ERROR: Timeout when waiting for text "+companyName+" in \""+selector+"\"");
				browser.end();
			}
		});
	},

	/**
	 * <b>TESTCASE</b><br>
	 * This function go to company tab<br>
	 * Check if company with name companyName is present in a list<br>
	 *
	 * @param {Object} browser          - nightwatch browser
	 * @param {String} [companyName]    - name of company which will be searching
	 * @param {requestCallback} [true/false, comapanyName] - The callback that returns: 
	 *									1) true(if company is present) or false(if company is not present)
	 *									2) companyName wich we can use for message or other actions
	 */
    'Find Company in table': function(browser, companyName, callback){

        // Go to companies list
        browser.globals.Helpers.goToLeftMenuItem(browser, 'list/companies');

        browser
        .waitForElementPresent('#dt_company', browser.globals.defaultTimeout, false)
        .waitForElementVisible('#dt_company', browser.globals.defaultTimeout, false)
        .execute(function(companyName) {
            var result = false;
            $('#dt_company tbody tr').each(function() {
                var td = $(this).find('td:nth-child(1)');
                if($.trim(td.text()) == companyName) {
                    result = true;
                    return false;
                } else {
                    result = false;
                };
            });
            return result;
        },[companyName], function(result) {
            callback(result.value, companyName);
        });

    },

};