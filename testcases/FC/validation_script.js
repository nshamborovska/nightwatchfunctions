/** @module FE/FC/testcases/validation_script */

// Modules
var Helpers = require('./../../includes/helpers.js');


module.exports = {

    /**
     * <b>TESTCASE</b><br>
     * Creates a new validation script.<br>
     * This testcase create a script with specific content.
     *
     * Requires:<br>
     * - browser.globals.url - with root url<br>
     * - browser.globals.Helpers - with pointer to <a href="module-includes_helpers.html">Helpers</a> module.
     *
     * @param {Object} browser          - nightwatch browser
     * @param {String} [scriptName]     - script name. Default: ATF_Script
     * @param {String} [scriptContent]  - Groovy Script. Default: script for validation of email
     * @param {String} [variables]       - variables for script. By default it's empty
     */
    'Create validation script': function (browser, scriptName, scriptContent, variables) {
        scriptName = scriptName || "ATF_Script";
        scriptContent = scriptContent || "emailPattern = /[_A-Za-z0-9-]+(\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\.[A-Za-z0-9-_]+)*(\.[A-Za-z0-9-_]{2,6}+)$/\nemail = value \nif(email ==~ emailPattern){\n}\nelse{ return [ERR_MAIL_FORMAT:\"Uw email adres is niet geldig\"] }";
        variables = variables || '';

        // Go to forms list
        browser.globals.Helpers.goToLeftMenuItem(browser, 'list/scripts');

        browser
            // Wait for forms list html loaded
            .waitForElementPresent('#btn_add_script', browser.globals.defaultTimeout)
            // Click add script button
            .click('#btn_add_script')
            // Wait for dialog visible
            .waitForElementVisible('#dialog_add_script', browser.globals.defaultTimeout)
            // Set form name
            .setValue('#dialog_add_script_name', scriptName)
            .setValue('#ace_editor_dialog_add_script_content textarea', scriptContent)
            .setValue('#dialog_add_script_variables', variables)

            // Click on submit
            .waitForElementVisible('.modal-footer .btn-success', browser.globals.defaultTimeout)
            .click('.modal-footer .btn-success', function () {
                console.log('Dialog submitted');
            });
            var selector = '#dt_scripts tbody tr td:nth-child(1)';
        browser.globals.Helpers.waitForText(browser, selector, scriptName, browser.globals.defaultTimeout, function (elementID) {
            if (!elementID) {
                browser.assert.title("ERROR", "ERROR: Timeout when waiting for text " + scriptName + " in \"" + selector + "\"");
                browser.end();
            }
        });

        return scriptName;
    },

    /**
     * <b>TESTCASE</b><br>
     * Find script in a Table<br>
     * Check if script with name scriptName is present in a list<br>
     *
     * @param {Object} browser          - nightwatch browser
     * @param {String} [scriptName]       - name of script which will be searching
     * @param {requestCallback} [true/false, scriptName] - The callback that returns: 
     *                                  1) true(if script is present) or false(if script is not present)
     *                                  2) scriptName wich we can use for message or other actions
     */

    'Find script in table': function (browser, scriptName, callback){
        // Go to scripts list
        browser.globals.Helpers.goToLeftMenuItem(browser, 'list/scripts');

        browser
        .waitForElementVisible('#dt_scripts', browser.globals.defaultTimeout)  
        .execute(function(scriptName) {
            var result = false;
            $('#dt_scripts tbody tr').each(function() {
                var td = $(this).find('td:nth-child(1)');
                if($.trim(td.text()) == scriptName) {
                    result = true;
                    return false;
                } else {
                    result = false;
                };
            });
            return result;
        },[scriptName], function(result) {
            callback(result.value, scriptName);
        });

    },


    /**
     * <b>TESTCASE</b><br>
     * Find script in table or create it<br>
     * Check if script with name scriptName is present in a list<br>
     * If script is present, the message in report will say about it.<br>
     * If script not present, the test will create it.<br>
     *
     * @param {Object} browser          - nightwatch browser
     * @param {String} [scriptName]    - name of script which will be searching/create
     * @param {String} [scriptContent] - content of script which will be searching/create
     */

    'Find script in table or create it': function (browser, scriptName, scriptContent){
        var t = this;
        t['Find script in table'](browser, scriptName, function(result, scriptName){
            if(result == true){
                browser
                .assert.ok(true, "Script with name "+scriptName+" present on FC");
            }else{
                browser
                .assert.ok(true, "Script with name "+scriptName+" not present on FC");
                t['Create validation script'](browser, scriptName, scriptContent);
            }
        });
    }

};