/** @module FE/FC/testcases/user */

// Modules
var Helpers = require('./../../includes/helpers.js');


module.exports = {

    /**
     * <b>TESTCASE</b><br>
     * Creates a user.<br><br>
     *
     * Requires:<br>
     * - browser.globals.url - with root url<br>
     * - browser.globals.Helpers - with pointer to <a href="module-includes_helpers.html">Helpers</a> module.
     *
     * @param {Object} browser          - nightwatch browser
     * @param {Array} [userRole]        - user role. Default: ROLE_QUEST_ADMIN
     * @param {String} [userName]       - user name. Default: ATF_User
     * @param {String} [userEmail]      - user email. Default: atf_user@example.com
     * @param {String} [userPassword]   - user password. Default: 12345678_Abc
     * @param {String} [companyName]    - company name. Default: ATF_Company
     */
    'Create user FC': function (browser, userName, userRole, userEmail, userPassword, companyName) {
        userName = userName || "ATF_User";
        userEmail = userEmail || "atf_user@example.com";
        companyName = companyName || "ATF_Company";
        userPassword = userPassword || "12345678_Abc";

        // Go to users list
        browser.globals.Helpers.goToLeftMenuItem(browser, 'list/users');

        browser
        // Wait for users list html loaded
        .waitForElementPresent('#btn_add_user', browser.globals.defaultTimeout)
        .waitForElementVisible('#btn_add_user', browser.globals.defaultTimeout)
        // Click add user button
        .click('#btn_add_user')
        // Wait for dialog visible
        .waitForElementPresent('#dialog_edit_user_name', browser.globals.defaultTimeout)
        .waitForElementVisible('#dialog_edit_user_name', browser.globals.defaultTimeout)
        // Set user name and email
        .setValue('#dialog_edit_user_name', userName)
        .setValue('#dialog_edit_user_email', userEmail)
        // Select company name
        .execute(function (companyName) {
            $('#dialog_edit_user_company option').filter(function () {
                return $(this).text() == companyName;
            }).prop('selected', true);
        }, [companyName])

        .execute(function (userRole) {
            $('#dialog_edit_user_role').val(userRole).trigger('change')
        }, [userRole])

        // Set password
    	.waitForElementVisible('#dialog_edit_user_password', browser.globals.defaultTimeout)
        .setValue('#dialog_edit_user_password', userPassword)
    	.waitForElementVisible('#dialog_edit_user .modal-footer button.btn-success', browser.globals.defaultTimeout)
        // Click on submit
        .click('#dialog_edit_user .modal-footer button.btn-success', function () {
            console.log("User dialog submitted");
        })
    },

    /**
     * <b>TESTCASE</b><br>
     * Check if the user is available.<br><br>
     *
     * Requires:<br>
     * - browser.globals.url - with root url<br>
     * - browser.globals.Helpers - with pointer to <a href="module-includes_helpers.html">Helpers</a> module.
     *
     * @param {Object} browser          - nightwatch browser
     * @param {requestCallback} [true/false] - The callback that returns: 
     *                                  1) true(if user was logined, error message not arise) or false(if user was not logined, error message arise)
     */

    'Check if the user is available' : function(browser, callback){

        browser.execute(function(browser) {
            var found = false;
            var errMessage = $("#login-form font:contains('Sorry, we were not able to find a user with that username and password')");
            if (errMessage.length > 0) {
                return false;
            } else {
                return true;
            }
        }, [], function(result) {
            if(callback) {
                callback(result.value);
            }
        });
            
    },

};
