/**  @module FE/FC/testcases/login */

var TestCases = {
    FC_User: require('./user.js'),
    FC_Company: require('./company.js'),
};

var UserRole = {
    globalAdmin: 'ROLE_ADMIN',
    companyAdmin: 'ROLE_COMP_ADMIN',
    companyUser: 'ROLE_QUEST_ADMIN'
};

	/**
	 * Call this function to submit FormsCreator login page with wrong
	 * data and expect error message
	 *
	 * @function
	 * @param  {Object} browser          - nightwatch browser
	 * @param  {String} expectedErrorMsg - expected error message
	 * @return {Object}                  nightwatch browser, so you can use this method in nightwatch chain
	 */
function submitWrong(browser, expectedErrorMsg) {
	browser
	.click('#login-form footer button')
	.waitForElementPresent('#login-form font .fa', browser.globals.defaultTimeout)
	.assert.containsText("#login-form font", expectedErrorMsg);
	return browser;
}

module.exports = {

	/**
	 * <b>TESTCASE</b><br>
	 * Opens a FormsCreator website and verifies it was rendered.
	 * URL depends on env parameter from command line, so it fails with error if it wasn't specified.
	 *
	 * @param {Object} browser - nightwatch browser
	 *
	 * @returns {String} site url
	 */
	'Open FC' : function(browser) {

		// Get URL
		var url = browser.globals.Settings.getFormsCreatorUrl(
			browser,
			browser.globals.CLI.getParam("---env")
		);

		browser
		.url(url)
		// Wait for username input field
		.waitForElementPresent('#j_username', browser.globals.defaultTimeout);

		return url;
	},


	/**
	 * <b>TESTCASE</b><br>
	 * Performs both unhappy and happy flows of FormsPublisher login.<br>
	 * Submits login page with/without wrong login/password.<br>
	 * At last step submits correct admin credentials and verifies that site was accessed.<br>
	 * At the end of test you are authorized inside application.
	 *
	 * @param {Object} browser          - nightwatch browser
	 * @param {Boolean} negativeTest    - true if you want to check also negative login scenarios
	 * @param {String} [login]          - correct login. Default: admin
	 * @param {String} [password]       - correct password. Default: adminL4tUS1n
	 */
	'Login to FC' : function(browser, negativeTest, login, password) {

		login = login || "admin";
		password = password || "adminL4tUS1n";

		var loginError = "Sorry, we were not able to find a user with that username and password.";

		browser
		.waitForElementPresent('#login-form footer button', browser.globals.defaultTimeout);

		// Perform negative tests if needed
		if(negativeTest) {
			browser
			// Submit without login and password
			submitWrong(browser, loginError)

			// Submit with wrong login and password
			.setValue('#j_username', 'wronglogin')
			.setValue('#j_password', 'wrongpassword')
			submitWrong(browser, loginError)

			// Submit with correct login but incorrect password
			.setValue('#j_username', login)
			.setValue('#j_password', 'wrongpassword')
			submitWrong(browser, loginError)

			// Submit with incorrect login but correct password
			.setValue('#j_username', 'wronglogin')
			.setValue('#j_password', password)
			submitWrong(browser, loginError);
		}

		// Submit with correct login and password
		browser
		.setValue('#j_username', login)
		.setValue('#j_password', password)
		.click('#login-form footer button')

		TestCases.FC_User['Check if the user is available'](browser, function(result) {
			if(result == true){
				browser.assert.ok(true, 'User found');
				browser
				.waitForElementPresent('#header .header-info', browser.globals.defaultTimeout, false)
				.waitForElementVisible('#header .header-info', browser.globals.defaultTimeout, false)
				// Verify that current user is displayed in header as logged in
				.verify.containsText('#header .header-info .info:last-child', login);
			}else{
				browser
				.assert.ok(true, "User is not found");
			}
		});
	},

	/**
	 * <b>TESTCASE</b><br>
	 * Performs logout.<br>
	 * Verifies that after logout login page is rendered.
	 *
	 * @param {Object} browser - nightwatch browser
	 */
	'Logout FC' : function(browser, negativeTest) {
		browser
		// Wait for logout button and click
		.waitForElementVisible('#logout', browser.globals.defaultTimeout)
		.jqueryClick('#logout a')
		// Wait for confirmation dialog
		.waitForElementVisible('#Msg1', browser.globals.defaultTimeout);

		if(negativeTest) {
			browser
			// Click on No
			.click('#Msg1 #bot2-Msg1')
			// Wait till confirmation dissapears
			.waitForElementNotVisible('#Msg1', browser.globals.defaultTimeout)
			// Click on logout again
			.jqueryClick('#logout a');
		}

		browser
		// Click on Yes
		.click('#Msg1 #bot2-Msg1')
		// Verify that login page is rendered
		.waitForElementVisible('#j_username', browser.globals.defaultTimeout);
		browser.globals.Helpers.deleteAllCookies(browser);
	},

	/**
	 * <b>TESTCASE</b><br>
	 * Performs login to FormsCreator.<br>
	 * In case when user was deleted before, the function will create it.<br>
	 * When user will be created, function ligins as thet user<br>
	 *
	 * @param {Object} browser          - nightwatch browser
	 * @param {String} [login]          - correct login. Default: admin
	 * @param {String} [password]       - correct password. Default: adminL4tUS1n
	 */
	'Login to FC as some static user': function(browser, login, password) {
    	var t = this;
    	var userRoles;
    	var password = password || "12345678_Abc";

    	if (login == "ATF_Global_Admin") {
    		userRoles = [UserRole.globalAdmin];
    	} else if (login == "ATF_Company_Admin") {
    		userRoles = [UserRole.companyAdmin];
    	} else if (login == "ATF_Company_User") {
    		userRoles = [UserRole.companyUser];
    	} else if (login == "ATF_Company_AdminAndUser") {
    		userRoles = [UserRole.companyAdmin,UserRole.companyUser];
    	} else {
            browser.assert.ok(false, 'No info about roles for user: '+ login);
            // if you see this error, you should add user roles for new user
    	};

    	t['Login to FC'](browser, false, login, password);
        TestCases.FC_User['Check if the user is available'](browser, function (result) {
            if (result == true) {
                browser.assert.ok(true, "User found");
                
            } else {
    			t['Login to FC'](browser, false);
                TestCases.FC_Company['Find Company in table'](browser, "ATF_Company", function (result, companyName) {
	                if (result == true) {
	                    browser.assert.ok(true, "Company with name: " + companyName + " is present");
	                } else {
	       				TestCases.FC_Company['Create company'](browser);
	                }
                	TestCases.FC_User['Create user FC'](browser, login, userRoles);
	                t['Logout FC'](browser);
					t['Login to FC'](browser, false, login, password);
	            });
        	}
    	});
    }
};






