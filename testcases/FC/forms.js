/** @module FE/FC/testcases/form */

var CLI = require('./../../includes/CLI.js');
var Helpers = require('./../../includes/helpers.js');

module.exports = {

    /**
     * <b>TESTCASE</b><br>
     * Creates a new form.<br>
     * After this testcase form will contain only root node.
     *
     * Requires:<br>
     * - browser.globals.url - with root url<br>
     * - browser.globals.Helpers - with pointer to <a href="module-includes_helpers.html">Helpers</a> module.
     *
     * @param {Object} browser          - nightwatch browser
     * @param {String} [formName]       - form name. Default: AT_Form
     */
    'Create a form': function (browser, formName, descriptionText) {

        formName = formName || 'ATF_Form';
        descriptionText = descriptionText || 'Description';

        // Go to forms list
        browser.globals.Helpers.goToLeftMenuItem(browser, 'list/questionnaires');

        browser
        // Wait for forms list html loaded
        .waitForElementVisible('#dt_questionnaires_wrapper', browser.globals.defaultTimeout)
        .waitForElementVisible('#btn_add_questionnaire', browser.globals.defaultTimeout)
        // Click add form button
        .click('#btn_add_questionnaire')
        // Wait for dialog visible
        .waitForElementVisible('#dialog_add_questionnaire', browser.globals.defaultTimeout)
        // Set form name
        .waitForElementVisible('#dialog_add_questionnaire_name', browser.globals.defaultTimeout)
        .setValue('#dialog_add_questionnaire_name', formName)
        // Set description
        .waitForElementVisible('#ace_editor_dialog_add_questionnaire_descr', browser.globals.defaultTimeout)
        .setValue('#ace_editor_dialog_add_questionnaire_descr textarea', descriptionText)
        // Click on submit
        .waitForElementVisible('#dialog_add_questionnaire .modal-footer button.btn-success', browser.globals.defaultTimeout)
        .click('#dialog_add_questionnaire .modal-footer button.btn-success', function () {
            console.log('Dialog submitted');
        })
        .waitForElementNotVisible('#dialog_add_questionnaire_name', browser.globals.defaultTimeout);
    },

    /**
     * <b>TESTCASE</b><br>
     * Import form<br>
     * Import form from file using link to file<br>
     * Prerequisite: browser must be on form tree page.<br>
     *
     * @param {Object} browser          - nightwatch browser
     * @param {String} [fileLink]       - link to file which should be imported
     */
    'Import form': function (browser, fileLink) {
        browser
            .waitForElementPresent('#btn_import', browser.globals.defaultTimeout)
            .click('#btn_import')
            .waitForElementPresent('.modal-content', browser.globals.defaultTimeout)
            .waitForElementVisible('.input-file', browser.globals.defaultTimeout)
            .setValue('.input-file input[type="file"]', __dirname + fileLink)
            .waitForElementVisible('#dialog_import .btn-primary', browser.globals.defaultTimeout)
            .click('#dialog_import .btn-primary')
            .waitForElementVisible('#divSmallBoxes', browser.globals.loadingTimeout)
            .waitForElementNotVisible('#divSmallBoxes', browser.globals.defaultTimeout)
            .waitForElementVisible('#tree_view', browser.globals.defaultTimeout)
    },

	/**
	 * <b>TESTCASE</b><br>
	 * Find form in a Table<br>
	 * Check if form with name formName is present in a list<br>
	 *
	 * @param {Object} browser          - nightwatch browser
	 * @param {String} [formName]    - name of form which will be searching
	 * @param {requestCallback} [true/false, formName] - The callback that returns: 
	 *									1) true(if form is present) or false(if form is not present)
	 *									2) formName wich we can use for message or other actions
	 */
    'Find form in a Table': function (browser, formName, callback) {
        browser.globals.Helpers.goToLeftMenuItem(browser, 'list/questionnaires');                
        browser
            .waitForElementVisible('#dt_questionnaires', browser.globals.defaultTimeout)
            .execute(function (formName) {
                var result = false;
                $('#dt_questionnaires tbody tr').each(function () {
                    var td = $(this).find('td:nth-child(2)');
                    if ($.trim(td.text()) == formName) {
                        result = true;
                        return false;
                    } else {
                        result = false;
                    }
                });
                return result;
            }, [formName], function (result) {
                callback(result.value, formName);
            });
    },


    /**
     * <b>TESTCASE</b><br>
     * Goes to form treeview screen.<br>
     *
     * Requires:<br>
     * - browser.globals.url - with root url<br>
     * - browser.globals.Helpers - with pointer to <a href="module-includes_helpers.html">Helpers</a> module.
     *
     * @param {Object} browser          - nightwatch browser
     * @param {String} [formName]       - form name. Default: AT_Form
     */
    'Go to form': function (browser, formName) {

        formName = formName || 'ATF_Form';

        // Go to forms list
        browser.globals.Helpers.goToLeftMenuItem(browser, 'list/questionnaires');

        browser
        // Wait for company table appear
            .waitForElementVisible('#dt_questionnaires tbody a.btn-edit-questionnaire', browser.globals.defaultTimeout)
        // Click on edit form button
        browser.globals.Helpers.clickButtonInTableRow(
            browser,
            formName,
            '#dt_questionnaires',
            'a.btn-edit-questionnaire'
        );
        browser
            .waitForElementVisible('#tree_view > ul', browser.globals.defaultTimeout);
    },

    /**
     * <b>TESTCASE</b><br>
     * Delete a form<br>
     * Verifies that form was successfully removed and not present on forms list anymore.<br><br>
     *
     * Requires:<br>
     * - browser.globals.url - with root url<br>
     * - browser.globals.Helpers - with pointer to <a href="module-includes_helpers.html">Helpers</a> module.
     *
     * @param {Object} browser          - nightwatch browser
     * @param {String} [formName]       - name of form to delete. Default: AT_Form
     */
    'Delete a form': function (browser, formName) {

        formName = formName || 'AT_Form';
        browser
        // Wait for forms table appear
            .waitForElementVisible('#dt_questionnaires tbody', browser.globals.defaultTimeout)

        // Click on delete form button
        browser.globals.Helpers.clickButtonInTableRow(
            browser,
            formName,
            '#dt_questionnaires',
            'a.btn-delete-questionnaire'
        );

        browser
        // Wait for confirmation dialog
            .waitForElementVisible('#Msg1', browser.globals.defaultTimeout)
            // Click on Yes
            .click('#Msg1 #bot2-Msg1')
            // Wait till table will dissappear
            .waitForElementNotPresent('#Msg1', browser.globals.defaultTimeout)

            // Wait till table will restart
            .waitForElementVisible('#dt_questionnaires tbody', browser.globals.defaultTimeout)
            .waitForElementVisible('#dt_questionnaires tbody', browser.globals.defaultTimeout);

        // Verify that form is not in the list anymore
        browser.globals.Helpers.waitForText(browser, '#dt_questionnaires tbody tr td:nth-child(2)', formName, browser.globals.defaultTimeout, function (elementID) {
            if (elementID) {
                browser.assert.title("ERROR", "ERROR: Form " + formName + " is still in the list after deleting");
                browser.end();
            } else {
                console.log('Verified: form ' + formName + ' is not in the list anymore');
            }
        });
    },

	/**
	 * <b>TESTCASE</b><br>
	 * This function go to form tab<br>
	 * Check if form with name formName is present in a list<br>
	 * If it present it will be deleted
	 *
	 * @param {Object} browser          - nightwatch browser
	 * @param {String} [formName]    - name of form which will be searching
	 */

    'Delete form if it is already exist': function (browser, formName) {
    	var t = this;
        // go to forms tab
        browser.globals.Helpers.goToLeftMenuItem(browser, 'list/questionnaires');
        t['Find form in a Table'](browser, formName, function (result, formName) {
            if (result == true) {
                t['Delete a form'](browser, formName);
                browser.assert.ok(true, 'Form with name: ' + formName + ' was present and remove');
            } else {
                browser.assert.ok(true, 'Form with name: ' + formName + ' is not present')
            }
        });
    },

    /**
     * <b>TESTCASE</b><br>
     * Publish the form to given publisher
     *
     * Prerequisite: browser must be on form tree page.
     *
     * @param {Object} browser          - nightwatch browser
     * @param {String} publisherName     - publisher name
     */
    'Publish form': function (browser, publisherName) {

        publisherName = publisherName || "ATF publisher"

        browser
        // Wait for button
            .waitForElementVisible('#btn_publish', browser.globals.defaultTimeout)
            // Click on button
            .click('#btn_publish')
            // Wait for dialog
            .waitForElementPresent('#publisherSelector', browser.globals.defaultTimeout)
            .waitForElementVisible('#publisherSelector label', browser.globals.defaultTimeout)
            // Check publishers checkboxes
            .execute(function (publisherName) {
                $('#publisherSelector label').each(function () {
                    var txt = $(this).text();
                    if (txt == publisherName) {
                        $(this).find('input').prop('checked', true);
                    }
                });
            }, [publisherName])
            .waitForElementVisible('#publish_from_modal_btn', browser.globals.defaultTimeout)
            // Submit
            .click('#publish_from_modal_btn')
            // Wait for text about success
            .waitForElementVisible('#publisherSelector .loader .txt-color-green', browser.globals.loadingTimeout)
            .assert.containsText("#publisherSelector .loader .txt-color-green", "Published !")
            // Close modal by clicking on cancel
            .waitForElementVisible('#dialog_publish .modal-footer .btn-danger', browser.globals.defaultTimeout)
            .click('#dialog_publish .modal-footer .btn-danger')
            .waitForElementNotVisible('#dialog_publish', browser.globals.defaultTimeout);
    },
};