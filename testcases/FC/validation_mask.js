/** @module FE/FC/testcases/validation_mask */

// Modules
var Helpers = require('./../../includes/helpers.js');


module.exports = {

    /**
     * <b>TESTCASE</b><br>
     * Creates a new validation mask.<br>
     * This testcase create a mask with specific content.
     *
     * Requires:<br>
     * - browser.globals.url - with root url<br>
     * - browser.globals.Helpers - with pointer to <a href="module-includes_helpers.html">Helpers</a> module.
     *
     * @param {Object} browser          - nightwatch browser
     * @param {String} [maskName]       - mask name. Default: ATF_Mask
     * @param {String} [maskContent]       - rule of mask. Default: 0000SS
     * @param {String} [maskOptions]       - options of mask. By default it's empty
     */
    'Create validation mask': function (browser, maskName, maskContent, maskOptions) {
        maskName = maskName || "ATF_Mask";
        maskContent = maskContent || "0000SS";
        maskOptions = maskOptions || '';

        // Go to forms list
        browser.globals.Helpers.goToLeftMenuItem(browser, 'list/masks');

        browser
            // Wait for forms list html loaded
            .waitForElementPresent('.no-mobile', browser.globals.defaultTimeout)
            // Click add mask button
            .click('.no-mobile')
            // Wait for dialog visible
            .waitForElementVisible('#dialog_add_mask', browser.globals.defaultTimeout)
            // Set mask name
            .setValue('#dialog_add_mask_name', maskName)
            .setValue('#dialog_add_mask_rule', maskContent)
            .setValue('#dialog_add_mask_options', maskOptions)

            // Click on submit
            .waitForElementVisible('.modal-footer .btn-success', browser.globals.defaultTimeout)
            .click('.modal-footer .btn-success', function () {
                console.log('Dialog submitted');
            });
            var selector = '#dt_masks tbody tr td:nth-child(1)';
        browser.globals.Helpers.waitForText(browser, selector, maskName, browser.globals.defaultTimeout, function (elementID) {
            if (!elementID) {
                browser.assert.title("ERROR", "ERROR: Timeout when waiting for text " + maskName + " in \"" + selector + "\"");
                browser.end();
            }
        });

        return maskName;
    },

    /**
     * <b>TESTCASE</b><br>
     * Find mask in a table<br>
     * Check if mask with name maskName is present in a list<br>
     *
     * @param {Object} browser          - nightwatch browser
     * @param {String} [maskName]    - name of mask which will be searching
     * @param {requestCallback} [true/false, maskName] - The callback that returns: 
     *                                  1) true(if mask is present) or false(if mask is not present)
     *                                  2) maskName wich we can use for message or other actions
     */

    'Find mask in table': function (browser, maskName, callback){
        // Go to masks list
        browser.globals.Helpers.goToLeftMenuItem(browser, 'list/masks');

        browser
        .waitForElementPresent('#dt_masks', browser.globals.defaultTimeout)  
        .waitForElementVisible('#dt_masks', browser.globals.defaultTimeout)  
        .execute(function(maskName) {
            var result = false;
            var rowNumber = 1;
            $('#dt_masks tbody tr').each(function() {
                var td = $(this).find('td:nth-child(1)');
                if($.trim(td.text()) == maskName) {
                    result = true;
                    return false;
                } else {
                    result = false;
                };
                rowNumber++;
            });
            return result;
        },[maskName], function(result) {
            callback(result.value, maskName);
        });

    },

    /**
     * <b>TESTCASE</b><br>
     * Find mask in table or create it<br>
     * Check if mask with name maskName is present in a list<br>
     * If mask is present, the message in report will say about it.<br>
     * If mask not present, the test will create it.<br>
     *
     * @param {Object} browser          - nightwatch browser
     * @param {String} [maskName]    - name of mask which will be searching/create
     * @param {String} [maskContent] - content of mask which will be searching/create
     */

    'Find mask in table or create it': function(browser, maskName, maskContent){
        var t = this;
        t['Find mask in table'](browser, maskName, function(result, maskName){
            if(result == true){
                browser
                .assert.ok(true, "Mask with name "+maskName+" present on FC");
            }else{
                browser
                .assert.ok(true, "Mask with name "+maskName+" not present on FC");
                t['Create validation mask'](browser, maskName, maskContent);
            }
        });
    }


};