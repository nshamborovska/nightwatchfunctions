/** @module FE/FC/testcases/form */

var CLI = require('./../../includes/CLI.js');
var Helpers = require('./../../includes/helpers.js');
var Settings = require('./../../includes/settings.js');


module.exports = {
    /**
     * <b>TESTCASE</b><br>
     * Creates a publisher.<br>
     * Verifies that new publisher was successfully created and present on publishers list.<br><br>
     *
     * Requires:<br>
     * - browser.globals.url - with root url<br>
     * - browser.globals.Helpers - with pointer to <a href="module-includes_helpers.html">Helpers</a> module.
     *
     * @param {Object} browser          - nightwatch browser
     * @param {String} publisherName    - name of new publisher
     * @param {String} publisherUrl     - url of new publisher
     * @param {String} publisherToken   - token of new publisher
     */
    'Create publisher' : function(browser, publisherName, publisherUrl, publisherToken) {

        // Get env
        var env = browser.globals.CLI.getParam('---env');
        // Get name
        publisherName = publisherName || browser.globals.Settings.getFormsPublisherName(browser, env);
        // Get url
        publisherUrl = publisherUrl || browser.globals.Settings.getFormsPublisherUrl(browser, env);
        // Get token
        publisherToken = publisherToken || browser.globals.Settings.getFormsPublisherToken(browser, env);


        browser.globals.Helpers.goToLeftMenuItem(browser, 'list/publishers');

        browser
        // Wait for publisher list html loaded
        .waitForElementVisible('#btn_add_publisher', browser.globals.defaultTimeout)
        // Click on add publisher button
        .click('#btn_add_publisher')
        // Wait for dialog is open
        .waitForElementVisible('#dialog_edit_publisher_name', browser.globals.defaultTimeout)
        // Set publisher name value to input
        .waitForElementVisible('#dialog_edit_publisher_name', browser.globals.defaultTimeout)
        .setValue('#dialog_edit_publisher_name',publisherName)
        // Set publisher url value to input
        .waitForElementVisible('#dialog_edit_publisher_url', browser.globals.defaultTimeout)
        .setValue('#dialog_edit_publisher_url',publisherUrl+'publisher/save')
        // Set publisher token value to input
        .waitForElementVisible('#dialog_edit_publisher_token', browser.globals.defaultTimeout)
        .setValue('#dialog_edit_publisher_token',publisherToken)
        // Click on submit
        .waitForElementVisible('#dialog_edit_publisher .modal-footer button.btn-success', browser.globals.defaultTimeout)
        .click('#dialog_edit_publisher .modal-footer button.btn-success', function() {
            console.log('Publisher dialog submit was clicked');
        });

        // Verify that publisher appeared in list
        var selector = '#dt_publishers tbody tr td:first-child';
        browser.globals.Helpers.waitForText(browser, selector, publisherName, browser.globals.loadingTimeout, function(elementID) {
            if(!elementID) {
                browser.assert.title("ERROR","ERROR: Timeout when waiting for text "+publisherName+" in \""+selector+"\"");
                browser.end();
            }
        });
    },

    /**
     * <b>TESTCASE</b><br>
     * Find publisher in table.<br>
     * Verifies that publisher is present in publishers list.<br><br>
     *
     * Requires:<br>
     * - browser.globals.url - with root url<br>
     * - browser.globals.Helpers - with pointer to <a href="module-includes_helpers.html">Helpers</a> module.
     *
     * @param {Object} browser          - nightwatch browser
     * @param {String} publisherName    - name of new publisher
     * @param {requestCallback} [true/false, publisherName] - The callback that returns: 
     *                                  1) true(if publisher is present) or false(if publisher is not present)
     *                                  2) publisherName wich we can use for message or other actions
     */

    'Find publisher in table': function (browser, publisherName, callback){
        // Go to publisher list
        browser.globals.Helpers.goToLeftMenuItem(browser, 'list/publishers');
        browser
        .waitForElementVisible('#dt_publishers', browser.globals.defaultTimeout)
        .execute(function(publisherName) {
            var result = false;
            $('#dt_publishers tbody tr').each(function() {
                var td = $(this).find('td:nth-child(1)');
                if($.trim(td.text()) == publisherName) {
                    result = true;
                    return false;
                } else {
                    result = false;
                };
            });
            return result;
        },[publisherName], function(result) {
            callback(result.value, publisherName);
        });

    },

    'Check if publisher is present': function (browser, publisherName) {
        var t = this;
        publisherName = publisherName || 'ATF publisher';
        t['Find publisher in table'](browser, "ATF publisher", function (result, publisherName) {
            if (result == true) {
                browser.assert.ok(true, 'Publisher with name: ' + publisherName + ' is present');
            } else {
                browser.assert.ok(true, 'Publisher with name: ' + publisherName + ' is not present, should be added');
                t['Create publisher'](browser);
            }
        });
    }

};

