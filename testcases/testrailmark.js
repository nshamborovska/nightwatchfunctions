/**  @module FE/testrailmark */

// Modules
var TestRail = require('./../includes/testRail.js');  // new line


module.exports = {

	'Push result to TestRail': function (browser, testName, caseID, messageOK, messageERR) {
	        if(browser.currentTest.results.testcases[testName].failed == 1) {
         // Mark it in TestRail as failed
             TestRail.setStatus(5, caseID, messageERR, function(err, response) {
                  if(err) {
                      // Add to report that you couldn't update status of this testcase in TestRail
                  }
             });
             return;
        }

        TestRail.setStatus(1, caseID, messageOK, function(err, response) {
             if(err) {
                 // Add to report that you couldn't update status of this testcase in TestRail
             }
        });  

	}
};