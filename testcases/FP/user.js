// Modules
var Helpers = require('./../../includes/helpers.js');

module.exports = {
    /**
     * <b>TESTCASE</b><br>
     * Creates a user.<br><br>
     *
     * Requires:<br>
     * - browser.globals.url - with root url<br>
     * - browser.globals.Helpers - with pointer to <a href="module-includes_helpers.html">Helpers</a> module.
     *
     * @param {Object} browser          - nightwatch browser
     * @param {String} [userName]       - user name. Default: ATF_User
     * @param {Array} [userRole]        - user role. Default: ROLE_QUEST_ADMIN
     * @param {String} [userEmail]      - user email. Default: atf_user@example.com
     * @param {String} [userPassword]   - user password. Default: 12345678_Abc
     * @param {String} [companyName]    - company name. Default: ATF_Company
     */

    'Create user FP': function(browser, userName, userRole, userEmail, userPassword, companyName) {
        userName = userName || "ATF_User";
        userEmail = userEmail || "atf_user@example.com";
        companyName = companyName || "ATF_Company";
        userPassword = userPassword || "12345678_Abc";

        // Go to users list
        browser.globals.Helpers.goToLeftMenuItem(browser, 'user/list');

        browser
        // Wait for users list html loaded
        .waitForElementPresent('#wid-id-0', browser.globals.defaultTimeout)
        .waitForElementVisible('#wid-id-0', browser.globals.defaultTimeout)
        // Click on add user button
        .waitForElementVisible('.widget-body-toolbar button.btn-primary', browser.globals.defaultTimeout)
        .click('.widget-body-toolbar button.btn-primary')
        // Wait for dialog is open
        .waitForElementVisible('#userModal', browser.globals.defaultTimeout)
        .waitForElementVisible('#username', browser.globals.defaultTimeout)
        // Set user name, email and password
        .setValue('#username', userName)
        .setValue('#userEmail', userEmail)
        .setValue('#userPassword', userPassword)

        // Select company name
        .execute(function (companyName) {
            $("#company option").filter(function () {
                return $(this).text() == companyName;
            }).prop('selected', true);
        }, [companyName])

        .execute(function (userRole) {
            $('#role').val(userRole).trigger('change')
        }, [userRole])
        // click submit button
        .waitForElementPresent('#userForm .modal-footer button.btn-success', browser.globals.defaultTimeout)
        .click('#userForm .modal-footer button.btn-success', function() {
            console.log('Company dialog submit was clicked');
        });
    },

    /**
     * <b>TESTCASE</b><br>
     * Check if the user is available.<br><br>
     *
     * Requires:<br>
     * - browser.globals.url - with root url<br>
     * - browser.globals.Helpers - with pointer to <a href="module-includes_helpers.html">Helpers</a> module.
     *
     * @param {Object} browser          - nightwatch browser
     * @param {requestCallback} [true/false] - The callback that returns: 
     *                                  1) true(if user was logined, error message not arise) or false(if user was not logined, error message arise)
     */

    'Check if the user is available' : function(browser, callback){

        browser.execute(function(browser) {
            var found = false;
            var errMessage = $("#login-form font:contains('Sorry, we were not able to find a user with that username and password')");
            if (errMessage.length > 0) {
                return false;
            } else {
                return true;
            }
        }, [], function(result) {
            if(callback) {
                callback(result.value);
            }
        });
            
    },

};