// Modules
var Helpers = require('./../../includes/helpers.js');

var TestCases = {
    FC_CreatePackageFromForm: require('./../../tests/FP_Packages/Create_package_from_form.js'),
};

module.exports = {


	/**
	 * <b>TESTCASE</b><br>
	 * Creates a new packages.<br>
     *
     * Requires:<br>
     * - browser.globals.url - with root url<br>
     * - browser.globals.Helpers - with pointer to <a href="module-includes_helpers.html">Helpers</a> module.
     *
     * @param {Object} browser          - nightwatch browser
     * @param {String} [formName]       - form name which should be use for creating package. Default: ATF_Form
     * @param {boolean} [actionType]       - depends on what final action will be implemented, create package or reject creating of package Default: true
     */

	'Create a package from form' : function(browser, formName, actionType) {

		formName = formName || "ATF_Form";
        actionType = actionType || 'Confirm';

		// Go to forms list
		browser.globals.Helpers.goToLeftMenuItem(browser, 'package/list');

		browser
		// Wait for add button
		.waitForElementPresent('.btn-primary', browser.globals.defaultTimeout)
		// Click add form button
		.click('.btn-primary')
		// Wait for dialog visible
		.waitForElementVisible('#packageForm', browser.globals.defaultTimeout)
		//choose form
		.waitForElementVisible('#newPackageFormSelect', browser.globals.defaultTimeout)
		.click('#newPackageFormSelect')
		.waitForElementVisible('#newPackageFormSelect option', browser.globals.defaultTimeout)
		.execute(function(formName) {
			$("#newPackageFormSelect option").filter(function() {
			    return $(this).text() == formName;
			}).prop('selected', true); 
		}, [formName]);
        if (actionType == 'Confirm'){
            browser
        	.click('#packageForm .btn-success', function() {
        		console.log('Dialog submitted');
        	})
        	// Wait till table will dissappear
        	.waitForElementNotVisible('#packageModal', browser.globals.defaultTimeout)
        	// Wait till table will appear again
        	.waitForElementVisible('#dt_packages tbody', browser.globals.defaultTimeout);
        	// Verify that package appeared in list
        	var selector = '#dt_packages tbody > tr > td:nth-child(2)';
        	browser.globals.Helpers.waitForText(browser, selector, formName, browser.globals.loadingTimeout, function(elementID) {
        		if(!elementID) {
        			browser.assert.title("ERROR","ERROR: Timeout when waiting for text "+formName+" in \""+selector+"\"");
        			browser.end();
        		}
        	});

		    return formName;
        }else if (actionType == 'Cancel'){
            browser
            .click('#packageForm .btn-danger', function() {
                console.log('Dialog was rejected');
            })
            .refresh()
            // Wait till table will dissappear
            .waitForElementNotVisible('#packageModal', browser.globals.defaultTimeout)
            // Wait till table will appear again
            .waitForElementVisible('#dt_packages tbody', browser.globals.defaultTimeout);           
            // Verify that package is not appeared in list
            var selector = '#dt_packages tbody > tr > td:nth-child(2)';
            browser.globals.Helpers.waitForText(browser, selector, formName, browser.globals.quickTimeout, function(elementID) {
                if(!elementID) {
                    browser.assert.ok(true, "Package is not displayed at list");
                }
            });
            return formName;
        }else{
                console.log('Action type is not correct');
        }
	},

	/**
	 * <b>TESTCASE</b><br>
	 * Create a Package if it is not exist.<br>
     *
     * Requires:<br>
     * - browser.globals.url - with root url<br>
     * - browser.globals.Helpers - with pointer to <a href="module-includes_helpers.html">Helpers</a> module.
     *
     * @param {Object} browser          - nightwatch browser
     * @param {String} [packageName]       - name of package which should be created if it's not exist. Default: ATF_Form_for_Packages
     */

    'Create a Package if it is not exist': function(browser, packageName) {
    	packageName = packageName || 'ATF_Form_for_Packages'
    	var t = this;
        t['Find package in table'](browser, packageName, function(result, formName) {
            if(result == true){
                browser
                .assert.ok(true, "Package present at FP");
            }else if(result == false){
                browser
                .assert.ok(true, "Package is not found at FP");
                TestCases.FC_CreatePackageFromForm['Check if form with name "ATF_Form_for_Packages" is present or create it'](browser);
            // will change this part before comit code
                t['Create a package from form'](browser, packageName);
            }else{
                browser
                .assert.ok(false, "Something wrong");
            }
        });

    },

    /**
     * <b>TESTCASE</b><br>
     * Import package from archive.<br>
     * Creating Package using import functionality.
     *
     * Requires:<br>
     * - browser.globals.url - with root url<br>
     * - browser.globals.Helpers - with pointer to <a href="module-includes_helpers.html">Helpers</a> module.
     *
     * @param {Object} browser          - nightwatch browser
     * @param {String} [importFile]       - name of zip file which should be used for creating package. Default: export-packages-Import_package.zip
     */
    'Import package from archive': function(browser, importFile){
        importFile = importFile || '/attachments/export-packages-Import_package.zip'
        browser.globals.Helpers.goToLeftMenuItem(browser, 'package/list');
        browser
        .waitForElementVisible('#dt_packages', browser.globals.defaultTimeout)
        .waitForElementVisible('#btn_import', browser.globals.defaultTimeout)
        .click('#btn_import')
        // check if import popup is opened
        .waitForElementVisible('.modal-dialog #package_importForm', browser.globals.defaultTimeout)
        .waitForElementVisible('.modal-dialog .modal-content #main_import_package_form', browser.globals.defaultTimeout)
        // choose file for import
        .setValue('input#zipArchive', require('path').resolve(__dirname + importFile))
        .click('.modal-footer button.btn-warning');
    },

	/**
	 * <b>TESTCASE</b><br>
	 * Find package in a table<br>
	 * Check if package with name packageName is present in a list<br>
	 *
	 * @param {Object} browser          - nightwatch browser
	 * @param {String} [packageName]    - name of package which will be searching
	 * @param {requestCallback} [true/false, packageName] - The callback that returns: 
	 *									1) true(if package is present) or false(if package is not present)
	 *									2) packageName wich we can use for message or other actions
	 */

    'Find package in table': function(browser, packageName, callback){
		packageName = packageName || 'ATF_Form';
        // Go to themes list
        browser.globals.Helpers.goToLeftMenuItem(browser, 'package/list');

        browser
        .waitForElementVisible('#dt_packages', browser.globals.defaultTimeout)
        .execute(function(packageName) {
            var result = false;
            $('#dt_packages tbody tr').each(function() {
                var td = $(this).find('td:nth-child(2)');
                if(td.text() == packageName) {
                    result = true;
                    return false;
                } else {
                    result = false;
                };
            });
            return result;
        },[packageName], function(result) {
            callback(result.value, packageName);
        });

    },

    /**
     * <b>TESTCASE</b><br>
     * Delete package if it is already exist<br>
     * Verifies that package not present in a list or remove it, if it's present<br><br>
     *
     * Requires:<br>
     * - browser.globals.url - with root url<br>
     *
     * @param {Object} browser          - nightwatch browser
     * @param {String} [packageName]       - name of package to delete.
     */

    'Delete package if it is already exist': function(browser, packageName) {
        var t = this;
        t['Find package in table'](browser, packageName, function(result, packageName){
            if( result == true ){
                t['Delete package'](browser, packageName);
                browser.assert.ok(true, 'package with name ' + packageName + ' was present and removed');
            } else {
                browser.assert.ok(true, 'package with name ' + packageName + ' is not present');
            };
        });
    },

    /**
     * <b>TESTCASE</b><br>
     * Delete package<br>
     * Verifies that package was successfully removed and not present on package list anymore.<br><br>
     *
     * Requires:<br>
     * - browser.globals.url - with root url<br>
     * - browser.globals.Helpers - with pointer to <a href="module-includes_helpers.html">Helpers</a> module.
     *
     * @param {Object} browser          - nightwatch browser
     * @param {String} [packageName]       - name of package to delete. Default: ATF_Form
     */

	'Delete package' : function(browser, packageName) {
		packageName = packageName || 'ATF_Form';
		// Go to package list
		browser.globals.Helpers.goToLeftMenuItem(browser, 'package/list');
		browser.waitForElementVisible('#dt_packages', browser.globals.defaultTimeout);
		browser.globals.Helpers.clickButtonInTableRow(
			browser, 
			packageName,
			'#dt_packages',
			'button.btn-danger'
		);
		browser
		.waitForElementVisible('#bot2-Msg1', browser.globals.defaultTimeout)
		.click('#bot2-Msg1', function() {
			console.log('Packege deleted');
		});
	},

    'Reject deletion of package': function(browser, packageName) {
        packageName = packageName || 'ATF_Form';
        // Go to package list
        browser.globals.Helpers.goToLeftMenuItem(browser, 'package/list');
        browser.waitForElementVisible('#dt_packages', browser.globals.defaultTimeout);
        browser.globals.Helpers.clickButtonInTableRow(
            browser, 
            packageName,
            '#dt_packages',
            'button.btn-danger'
        );
        browser
        .waitForElementVisible('#bot2-Msg1', browser.globals.defaultTimeout)
        .click('#bot1-Msg1', function() {
            console.log('Package deleting was rejected');
        });    
    },

	/**
	 * <b>TESTCASE</b><br>
	 * Check if package is not in a list<br>
	 * Check if package with name packageName is present in a list<br>
	 *
	 * @param {Object} browser          - nightwatch browser
	 * @param {String} [packageName]    - name of package which will be searching
	 */
    'Check if package is not in a list': function(browser, packageName) {
        var t = this;
        t['Find package in table'](browser, packageName, function(result, packageName) {
            if(result == false){
                browser
                .assert.ok(true, "Package is not in a list anymore");
            }else if(result == true) {
                browser
                .assert.ok(false, "Package was\'t deleted, it is still in a list");
            }else{
                browser
                .assert.ok(false, "Something wrong with test");
            }
        });
    },

};