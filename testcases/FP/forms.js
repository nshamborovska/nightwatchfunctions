/** @module FE/FC/testcases/form */

var CLI = require('./../../includes/CLI.js');
var Helpers = require('./../../includes/helpers.js');

module.exports = {


	/**
	 * <b>TESTCASE</b><br>
	 * Find form in a Table<br>
	 * Check if form with name formName is present in a list<br>
	 *
	 * @param {Object} browser          - nightwatch browser
	 * @param {String} [formName]    - name of company which will be searching
	 * @param {requestCallback} [true/false, frmName] - The callback that returns: 
	 *									1) true(if form is present) or false(if form is not present)
	 *									2) formName wich we can use for message or other actions
	 */
    'Find form in a Table': function (browser, formName, callback) {
        // Go to forms list
        browser.globals.Helpers.goToLeftMenuItem(browser, '');

        browser
        .waitForElementVisible('#dt_basic', browser.globals.defaultTimeout) 
        .execute(function(formName) {
            var result = false;
            $('#dt_basic tbody tr').each(function() {
                var td = $(this).find('td:nth-child(1)');
                if(td.text() == formName) {
                    result = true;
                    return false;
                } else {
                    result = false;
                };
            });
            return result;
        },[formName], function(result) {
            callback(result.value, formName);
        });
    },
};

