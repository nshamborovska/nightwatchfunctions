/** @module FE/FC/testcases/company */

// Modules
var Helpers = require('./../../includes/helpers.js');

module.exports = {

	/**
	 * <b>TESTCASE</b><br>
	 * Creates a company.<br>
	 * Verifies that new company was successfully created and present on companies list.<br><br>
	 *
	 * Requires:<br>
	 * - browser.globals.url - with root url<br>
	 * - browser.globals.Helpers - with pointer to <a href="module-includes_helpers.html">Helpers</a> module.
	 *
	 * @param {Object} browser          - nightwatch browser
	 * @param {String} [companyName]    - name of new company. Default: AT_Company
	 */
	'Create company' : function(browser, companyName, companyDescription) {

		companyName = companyName || "ATF_Company";
		companyDescription = companyDescription || "";

		// Go to companies
		browser.globals.Helpers.goToLeftMenuItem(browser, 'company/list');

		// TODO unhappy flows

		browser
		// Wait for company list html loaded
		.waitForElementPresent('#wid-id-0', browser.globals.defaultTimeout)
		.waitForElementVisible('#wid-id-0', browser.globals.defaultTimeout)
		// Click on add company button
		.waitForElementVisible('.widget-body-toolbar .row .text-right button.btn-primary', browser.globals.defaultTimeout)
		.click('.widget-body-toolbar .row .text-right button.btn-primary')
		// Wait for dialog is open
		.waitForElementVisible('#companyModal', browser.globals.defaultTimeout)
		// Set company's name value to input
		.waitForElementVisible('#companyName', browser.globals.defaultTimeout)
		.setValue('#companyName', companyName)
		// Set company's description value to input
		.waitForElementVisible('#companyDescription', browser.globals.defaultTimeout)
		.setValue('#companyDescription', companyDescription)
		// Click on submit
		.waitForElementVisible('#companyForm .modal-footer button.btn-success', browser.globals.defaultTimeout)
		.click('#companyForm .modal-footer button.btn-success', function() {
			console.log("Company dialog submit was clicked");
		});
		// Verify that company appeared in list
		var selector = '#dt_basic tbody tr td:first-child';
		browser.globals.Helpers.waitForText(browser, selector, companyName, browser.globals.loadingTimeout, function(elementID) {
			if(!elementID) {
				browser.assert.title("ERROR","ERROR: Timeout when waiting for text "+companyName+" in \""+selector+"\"");
				browser.end();
			}
		});
	},


	/**
	 * <b>TESTCASE</b><br>
	 * Find Company in table.<br>
	 * Check if company is present in companyes table.<br><br>
	 *
	 * @param {Object} browser          - nightwatch browser
	 * @param {String} [companyName]    - name of company which will be searching
	 * @param {requestCallback} [true/false, companyName] - The callback that returns: 
	 *									1) true(if company is present) or false(if company is not present)
	 *									2) companyName wich we can use for message or other actions
	 */

    'Find Company in table': function (browser, companyName, callback){
        // Go to companies list
        browser.globals.Helpers.goToLeftMenuItem(browser, 'company/list');

        browser
        .waitForElementVisible('#dt_basic', browser.globals.defaultTimeout) 
        .waitForElementVisible('#dt_basic tbody tr td:nth-child(1)', browser.globals.defaultTimeout) 
        .execute(function(companyName) {
            var result = false;
            $('#dt_basic tbody tr').each(function() {
                var td = $(this).find('td:nth-child(1)');
                if(td.text() == companyName) {
                    result = true;
                    return false;
                } else {
                    result = false;
                };
            });
            return result;
        },[companyName], function(result) {
            callback(result.value, companyName);
        });
    },

	/**
	 * <b>TESTCASE</b><br>
	 * Create a Company if it is not exist.<br>
	 * Check if company is present in companyes table and if company isn't exist, it will create company.<br><br>
	 *
	 * @param {Object} browser          - nightwatch browser
	 * @param {String} [companyName]    - name of company which will be searching and creating. Default: ATF_Company
	 */
	'Create a Company if it is not exist': function(browser, companyName){
		companyName = companyName || "ATF_Company"
		var t = this;
		t['Find Company in table'](browser, companyName, function (result, companyName) {
            if (result == true) {
                browser.assert.ok(true, 'Company with name: ' + companyName + ' is present');
            } else {
            	t['Create company'](browser, companyName);
                browser.assert.ok(true, 'Company with name: ' + companyName + ' was created')
            };
        });	
	},

	/**
	 * <b>TESTCASE</b><br>
	 * Delete a company.<br>
	 * Verifies that company was successfully removed and not present on companies list anymore.<br><br>
	 *
	 * Requires:<br>
	 * - browser.globals.url - with root url<br>
	 * - browser.globals.Helpers - with pointer to <a href="module-includes_helpers.html">Helpers</a> module.
	 *
	 * @param {Object} browser          - nightwatch browser
	 * @param {String} [companyName]    - name of company to delete. Default: ATF_Company
	 */
	'Delete Company': function(browser, companyName) {
		companyName =  companyName ||"ATF_Company";
		// Go to companies list
		browser.globals.Helpers.goToLeftMenuItem(browser, 'company/list');

		browser
		// Wait for company table appear
		.waitForElementVisible('#dt_basic_wrapper tbody', browser.globals.defaultTimeout)
        .waitForElementVisible('#dt_basic button.btn-danger',  browser.globals.defaultTimeout);
		// Click on delete company button
		browser.globals.Helpers.clickButtonInTableRow(
			browser,
			companyName,
			'#dt_basic_wrapper',
			'button.btn-danger'
		);
		browser
		// Wait for confirmation dialog
		.waitForElementVisible('#delete_company_confirmed_btn', browser.globals.defaultTimeout)
		// Click on Yes
		.click('#delete_company_confirmed_btn')
		// Wait till table will dissappear
		.waitForElementNotVisible('#delete_company_confirmed_btn', browser.globals.defaultTimeout)
		// Wait till table will appear again
        .refresh()
		.waitForElementVisible('#dt_basic', browser.globals.defaultTimeout);

		// Verify that company is not in the list anymore
        browser.globals.Helpers.waitForText(browser, '#dt_basic tbody tr td:nth-child(1)', companyName, browser.globals.defaultTimeout, function(elementID) {
            if(elementID) {
                browser.assert.title("ERROR","ERROR: Company "+companyName+" is still in the list after deleting");
                browser.end();
            } else {
                console.log('Verified: Company '+companyName+' is not in the list anymore');
            }
		});
	}, 

	'Delete Company if it is already exist': function(browser, companyName) {
		companyName =  companyName ||"ATF_Company";
		var t = this;
		// Go to companies list
		browser.globals.Helpers.goToLeftMenuItem(browser, 'company/list');
		t['Find Company in table'](browser, companyName, function(result, companyName){
            if (result == true) {
                browser.assert.ok(true, "Company with name: " + companyName + " is present");
                t['Delete Company'](browser, companyName);
            } else {
                browser.assert.ok(true, "Company with name: " + companyName + " is not present");
            }
		});
	}

};