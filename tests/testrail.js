/**  @module FE/tests/regression */

// Modules
var CLI = require('./../includes/cli.js');
var Helpers = require('./../includes/helpers.js');
var TreeHelpers = require('./../includes/treeHelpers.js');
var Settings = require('./../includes/settings.js');

// Testcases
var TestCases = {
    FP_Login: require('./../testcases/FP/login.js'),
    FP_Company: require('./../testcases/FP/company.js'),
    TestRail: require('./../testcases/testrailmark.js'),
};

module.exports = {

    before: function (browser) {
        browser.globals.CLI = CLI;
        browser.globals.Helpers = Helpers;
        browser.globals.TreeHelpers = TreeHelpers;
        browser.globals.Settings = Settings;
        browser.resizeWindow(1400, 800);
    },

    after: function (browser) {
        TestCases.TestRail['Push result to TestRail'](browser, 'Check if company was created', 62, 'Company with name ATF_Company was created', 'test was faild');  
        TestCases.TestRail['Push result to TestRail'](browser, 'Check if company was deleted', 31, 'Company with name ATF_Company was removed', 'test was failed');  
        TestCases.TestRail['Push result to TestRail'](browser, 'Failed test', 32, 'Package was created', 'test was failed, package wasn\'t created');  
    },

// Case 1

    'Open Forms Publisher for creating': function(browser) {
        browser.globals.url = TestCases.FP_Login['Open FP'](browser);
    },

    'Login to Forms Publisher as Admin for creating': function(browser){
        TestCases.FP_Login['Login to FP'](browser)
    },

    'Create a company with valid name': function(browser) {
        TestCases.FP_Company['Remove Company if it exists'](browser, 'ATF_Company');
        TestCases.FP_Company['Create company'](browser, 'ATF_Company');
    },

    'Check if company was created': function(browser) {
        companyName = 'ATF_Company';
        TestCases.FP_Company['Find Company in table'](browser, companyName, function (result, companyName) {
            if (result == true) {
                browser.assert.ok(true, 'Company with name: ' + companyName + ' is present');
            } else {
                browser.assert.ok(false, 'Company with name: ' + companyName + ' is not present')
            };        });  
    },
    'Close browser 1': function(browser) {
        browser.end();
    },
// Case 2

    'Open Forms Publisher for removing': function(browser) {
        browser.globals.url = TestCases.FP_Login['Open FP'](browser);
    },

    'Login to Forms Publisher as Admin for removing': function(browser){
        TestCases.FP_Login['Login to FP'](browser)
    },

    'Delete a company': function(browser) {
        TestCases.FP_Company['Create a Company if it is not exist'](browser, 'ATF_Company');
        TestCases.FP_Company['Remove Company'](browser, 'ATF_Company');
    },

    'Check if company was deleted': function(browser) {
        companyName = 'ATF_Company';
        TestCases.FP_Company['Find Company in table'](browser, companyName, function (result, companyName) {
            if (result == true) {
                browser.assert.ok(false, 'Company with name: ' + companyName + ' is present');
            } else {
                browser.assert.ok(true, 'Company with name: ' + companyName + ' is not present')
            };
        });

    },

    'Failed test': function(browser) {
        browser.assert.ok(false, 'Create package from form is failed');
    }, 

    'Close browser 2': function(browser) {
        browser.end();
    }


}










