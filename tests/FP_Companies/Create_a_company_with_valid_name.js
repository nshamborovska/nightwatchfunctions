/**  @module FE/tests/regression */
/**
 * Created by N.Shamborovska on 29.11.17.
 * File name: Create_a_company_with_valid_name.js
 * Full link for test: tests/FP_Companies/Create_a_company_with_valid_name.js
 */

// Modules
var CLI = require('./../../includes/cli.js');
var Helpers = require('./../../includes/helpers.js');
var TreeHelpers = require('./../../includes/treeHelpers.js');
var Settings = require('./../../includes/settings.js');

// Testcases
var TestCases = {
    FP_Login: require('./../../testcases/FP/login.js'),
    FP_Company: require('./../../testcases/FP/company.js'),
    TestRail: require('./../../testcases/testrailmark.js'),

};

module.exports = {

    before: function (browser) {
        browser.globals.CLI = CLI;
        browser.globals.Helpers = Helpers;
        browser.globals.TreeHelpers = TreeHelpers;
        browser.globals.Settings = Settings;
        browser.resizeWindow(1400, 800);
    },

    after: function (browser) {
        TestCases.TestRail['Push result to TestRail'](browser, 'Check if Company with name ATF_Company was created. Tested by Global admin', 49, 'Company was created by GA user', 'test was failed, company wasn\'t created');  
    },

// Case 49  Create a company with valid name

    // User = GA
    'Open Forms Publisher': function(browser) {
        browser.globals.url = TestCases.FP_Login['Open FP'](browser);
    },

    'Login to Forms Publisher as GA': function(browser) {
        TestCases.FP_Login['Login to FP as some static user'](browser, 'ATF_Global_Admin');
    },

    'Check if company with name ATF_Company is not present': function(browser) {
        TestCases.FP_Company['Delete Company if it is already exist'](browser, 'ATF_Company');
    },

    'Create a company': function(browser) {
        TestCases.FP_Company['Create company'](browser, 'ATF_Company', 'ATF Description');
    },

    'Check if Company with name ATF_Company was created': function(browser) {
        TestCases.FP_Company['Find Company in table'](browser, 'ATF_Company', function(result, companyName) {
            if (result == true) {
                browser.assert.ok(true, "Company with name: " + companyName + " is present");
            } else {
                browser.assert.ok(false, "Company with name: " + companyName + " is not present, expected: present");
            }
        });
    }, 

    'Close browser': function(browser) {
        browser.end();
    }, 

}










