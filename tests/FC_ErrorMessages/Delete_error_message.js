/**  @module FE/tests/regression */

// Modules
var CLI = require('./../../includes/cli.js');
var Helpers = require('./../../includes/helpers.js');
var TreeHelpers = require('./../../includes/treeHelpers.js');
var Settings = require('./../../includes/settings.js');

// TestCases
var TestCases = {
    FC_Login: require('./../../testcases/FC/login.js'),
    FC_ErrorMessages: require('./../../testcases/FC/error_messages.js'),
    TestRail: require('./../../testcases/testrailmark.js'),
};

var errorName = "ERR_FOR_UPDATE";
var errorContent = "Dit veld is verplicht";

module.exports = {

	before: function (browser) {
        browser.globals.CLI = CLI;
        browser.globals.Helpers = Helpers;
        browser.globals.TreeHelpers = TreeHelpers;
        browser.globals.Settings = Settings;
        browser.resizeWindow(1400, 800);
    },

    after: function (browser) {  
        TestCases.TestRail["Push result to TestRail"](browser, "Check if error message is not in the list anymore", 96, "Error message was deleted by CA&CU user", "Test was failed, error message wasn\'t deleted");
        TestCases.TestRail["Push result to TestRail"](browser, "Check if error message is not in the list anymore once more", 96, "Error message was deleted by CU user", "Test was failed, error message wasn\'t deleted"); 
    },

// Case 96 Delete error message

    // User = CA&CU
    "Open FormsCreator": function (browser) {
        browser.globals.url = TestCases.FC_Login["Open FC"](browser);
    },

    "Login to FC as CA&CU" : function (browser) {
        TestCases.FC_Login["Login to FC as some static user"](browser, "ATF_Company_AdminAndUser");
    },

    "Check if error message is present on the list of messages. If not - create it": function (browser) {
        TestCases.FC_ErrorMessages["Find error message in table or create it"](browser, errorName, errorContent);
    },

    "Delete error message": function (browser) {
        TestCases.FC_ErrorMessages["Delete error message"](browser, errorName);
    },

    "Check if error message is not in the list anymore": function (browser) {
        TestCases.FC_ErrorMessages["Check if error message is not in the list anymore"](browser, errorName);
    },

    "Log out from FC as CA&CU": function (browser) {
        TestCases.FC_Login["Logout FC"](browser);
    },

    // User = CU
    "Open FormsCreator once more": function (browser) {
        this["Open FormsCreator"](browser);
    },

    "Login to FC as CU user": function (browser) {
        TestCases.FC_Login["Login to FC as some static user"](browser, "ATF_Company_User");
    },

    "Create error message": function (browser) {
        TestCases.FC_ErrorMessages["Create error message"](browser, errorName);
    },

    "Delete error message once more": function (browser) {
        this["Delete error message"](browser);
    },

    "Check if error message is not in the list anymore once more": function (browser) {
        this["Check if error message is not in the list anymore"](browser);
    },

    "Close browser": function(browser) {
        browser.end();
    },

}