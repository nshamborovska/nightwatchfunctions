/**  @module FE/tests/regression */

// Modules
var CLI = require('./../../includes/cli.js');
var Helpers = require('./../../includes/helpers.js');
var TreeHelpers = require('./../../includes/treeHelpers.js');
var Settings = require('./../../includes/settings.js');

// TestCases
var TestCases = {
    FC_Login: require('./../../testcases/FC/login.js'),
    FC_ErrorMessages: require('./../../testcases/FC/error_messages.js'),
    TestRail: require('./../../testcases/testrailmark.js'),
};

var errorName = "ERR_FOR_UPDATE";
var errorContent = "Dit veld is verplicht";

module.exports = {

	before: function (browser) {
        browser.globals.CLI = CLI;
        browser.globals.Helpers = Helpers;
        browser.globals.TreeHelpers = TreeHelpers;
        browser.globals.Settings = Settings;
        browser.resizeWindow(1400, 800);
    },

    after: function (browser) {  
        TestCases.TestRail["Push result to TestRail"](browser, "Check if new error message is present on the list of error messages", 419, "Error message was created by CA&CU user", "Test was failed, error message wasn\'t created");
    },

// Case 419 Create error message for update

    "Open FormsCreator": function(browser) {
        browser.globals.url = TestCases.FC_Login["Open FC"](browser);
    },

    "Login to FC as CA&CU" : function(browser){
    	TestCases.FC_Login['Login to FC as some static user'](browser, "ATF_Company_AdminAndUser");
    },

    "Delete error message if it already exists": function (browser) {
        TestCases.FC_ErrorMessages["Delete error message if it already exists"](browser, errorName);
    },

    "Create error message": function (browser) {
        TestCases.FC_ErrorMessages["Create error message"](browser, errorName, errorContent);
    },

    "Check if new error message is present on the list of error messages": function (browser) {
        TestCases.FC_ErrorMessages["Find error message in table"](browser, errorName, function (result, errorName) {
            if (result == true ) {
                browser.assert.ok (true, "Error message with name " + errorName + " is present");
            } else {
                browser.assert.ok (false, "Error message with name " + errorName + " is not present, expected: present");
            };
        });
    },  

    'Close browser': function(browser) {
    	browser.end();
    }
}    
