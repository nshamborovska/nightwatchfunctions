/**  @module FE/tests/regression */
/**
 * Created by N.Shamborovska on 24.11.17.
 * File name: Delete_package.js
 * Full link for test: tests/FP_Packages/Delete_package.js
 */

// Modules
var CLI = require('./../../includes/cli.js');
var Helpers = require('./../../includes/helpers.js');
var TreeHelpers = require('./../../includes/treeHelpers.js');
var Settings = require('./../../includes/settings.js');

// Testcases
var TestCases = {
    FP_Login: require('./../../testcases/FP/login.js'),
    FP_Packages: require('./../../testcases/FP/packages.js'),
    TestRail: require('./../../testcases/testrailmark.js'),
};

module.exports = {

    before: function (browser) {
        browser.globals.CLI = CLI;
        browser.globals.Helpers = Helpers;
        browser.globals.TreeHelpers = TreeHelpers;
        browser.globals.Settings = Settings;
        browser.resizeWindow(1400, 800);
    },

    after: function (browser) {
        TestCases.TestRail['Push result to TestRail'](browser, 'Check if package is not in a list anymore', 46, 'Package was deleted by CA&CU user', 'test was failed, package wasn\'t deleted');  
        TestCases.TestRail['Push result to TestRail'](browser, 'Check if package is not in a list anymore once more', 46, 'Package was deleted by CA user', 'test was failed, package wasn\'t deleted');  
    },

// Case 46  Delete package

    // User = CA&CU
    'Open Forms Publisher for creating': function(browser) {
        browser.globals.url = TestCases.FP_Login['Open FP'](browser);
    },

    'Login to Forms Publisher as CA&CU': function(browser){
        TestCases.FP_Login['Login to FP as some static user'](browser, 'ATF_Company_AdminAndUser')
    },

    'Check if package with name "ATF_Form_for_Packages" is present': function(browser) {
        TestCases.FP_Packages['Create a Package if it is not exist'](browser, 'ATF_Form_for_Packages');
    },

    'Delete package': function(browser) {
        TestCases.FP_Packages['Delete package'](browser, 'ATF_Form_for_Packages');
    },

    'Check if package is not in a list anymore': function(browser) {
        TestCases.FP_Packages['Check if package is not in a list'](browser, 'ATF_Form_for_Packages');
    },

    'Close browser': function(browser) {
        browser.end();
    }, 

    // User = CA
    'Open Forms Publisher for creating once more': function(browser) {
        this['Open Forms Publisher for creating'](browser);
    },

    'Login to Forms Publisher as CA': function(browser){
        TestCases.FP_Login['Login to FP as some static user'](browser, 'ATF_Company_Admin')
    },

    'Create package from form': function(browser) {
        TestCases.FP_Packages['Create a package from form'](browser, 'ATF_Form_for_Packages');
    },

    'Delete package once more': function(browser) {
        this['Delete package'](browser);
    },

    'Check if package is not in a list anymore once more': function(browser) {
        this['Check if package is not in a list anymore'](browser);
    },

    'Close browser as CA': function(browser) {
        this['Close browser'](browser);
    }, 

}










