/**  @module FE/tests/regression */
/**
 * Created by N.Shamborovska on 29.11.17.
 * File name: To_not_delete_package.js
 * Full link for test: tests/FP_Packages/To_not_delete_package.js
 */
 
// Modules
var CLI = require('./../../includes/cli.js');
var Helpers = require('./../../includes/helpers.js');
var TreeHelpers = require('./../../includes/treeHelpers.js');
var Settings = require('./../../includes/settings.js');

// Testcases
var TestCases = {
    FP_Login: require('./../../testcases/FP/login.js'),
    FP_Packages: require('./../../testcases/FP/packages.js'),
    TestRail: require('./../../testcases/testrailmark.js'),
};

module.exports = {

    before: function (browser) {
        browser.globals.CLI = CLI;
        browser.globals.Helpers = Helpers;
        browser.globals.TreeHelpers = TreeHelpers;
        browser.globals.Settings = Settings;
        browser.resizeWindow(1400, 800);
    },

    after: function (browser) {
        TestCases.TestRail['Push result to TestRail'](browser, 'Check if package is present in a list', 47, 'Package wasn\'t deleted by CA&CU user', 'Test was failed, package was deleted');  
    },

// Case 47 To not delete package

    // User = CA&CU
    'Open Forms Publisher for creating': function(browser) {
        browser.globals.url = TestCases.FP_Login['Open FP'](browser);
    },

    'Login to Forms Publisher as CA&CU': function(browser){
        TestCases.FP_Login['Login to FP as some static user'](browser, 'ATF_Company_AdminAndUser')
    },

    'Check if package with name "ATF_Form_for_Packages" is present': function(browser) {
        TestCases.FP_Packages['Create a Package if it is not exist'](browser, 'ATF_Form_for_Packages');
    },

    'Reject deletion of package': function(browser) {
        TestCases.FP_Packages['Reject deletion of package'](browser, 'ATF_Form_for_Packages'); 
    },

    'Check if package is present in a list': function(browser) {
        TestCases.FP_Packages['Find package in table'](browser, 'ATF_Form_for_Packages', function(result, packageName){
            if( result == true ){
                browser.assert.ok(true, 'Package with name ' + packageName + ' is present');
            } else {
                browser.assert.ok(false, 'Package with name ' + packageName + ' is not present, expected: present');
            };
        });

    },

    'Close browser': function(browser) {
        browser.end();
    }, 
}










