/**  @module FE/tests/regression */
/**
 * Created by N.Shamborovska on 12.12.17.
 * File name: To_not_create_package.js
 * Full link for test: tests/FP_Packages/To_not_create_package.js
 */

// Modules
var CLI = require('./../../includes/cli.js');
var Helpers = require('./../../includes/helpers.js');
var TreeHelpers = require('./../../includes/treeHelpers.js');
var Settings = require('./../../includes/settings.js');

// Testcases
var TestCases = {
    FP_Login: require('./../../testcases/FP/login.js'),
    FP_Packages: require('./../../testcases/FP/packages.js'),
    TC_CreatePackageFromForm: require('./Create_package_from_form.js'),
    TestRail: require('./../../testcases/testrailmark.js'),
};

module.exports = {

    before: function (browser) {
        browser.globals.CLI = CLI;
        browser.globals.Helpers = Helpers;
        browser.globals.TreeHelpers = TreeHelpers;
        browser.globals.Settings = Settings;
        browser.resizeWindow(1400, 800);
    },

    after: function (browser) {
       TestCases.TestRail['Push result to TestRail'](browser, 'Check if package was not created', 30, 'Package from form wasn\'t created. Tested by CA&CU user', 'Test was failed, package was created.');  
    },

// Case 30  To not create package

    // User = CA&CU
    'Open Forms Publisher for creating': function(browser) {
        browser.globals.url = TestCases.FP_Login['Open FP'](browser);
    },

    'Login to Forms Publisher as CA&CU': function(browser){
        TestCases.FP_Login['Login to FP as some static user'](browser, 'ATF_Company_AdminAndUser')
    },

    'Check if form with name "ATF_Form_for_Packages" is or create it': function(browser) {
        TestCases.TC_CreatePackageFromForm['Check if form with name "ATF_Form_for_Packages" is or create it'](browser);
    },

    'Delete package if it is already exist': function(browser) {
        TestCases.FP_Packages['Delete package if it is already exist'](browser, 'ATF_Form_for_Packages');
    },

    'Create a package and reject creating': function(browser) {
        TestCases.FP_Packages['Create a package from form'](browser, 'ATF_Form_for_Packages', 'Cancel');
    },

    'Check if package was not created': function(browser) {
        TestCases.FP_Packages['Find package in table'](browser, 'ATF_Form_for_Packages', function(result){
            if( result == true ){
                browser.assert.ok(false, 'Package is present. Expected: not present');
            } else if( result == false){
                browser.assert.ok(true, 'Package is not present.');
            }
        });
    },

    'Close browser': function(browser) {
        browser.end();
    }
}