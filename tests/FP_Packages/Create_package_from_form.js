/**  @module FE/tests/regression */
/**
 * Created by N.Shamborovska on 30.10.17.
 * File name: Create_package_from_form.js
 * Full link for test: tests/FP_Packages/Create_package_from_form.js
 */

// Modules
var CLI = require('./../../includes/cli.js');
var Helpers = require('./../../includes/helpers.js');
var TreeHelpers = require('./../../includes/treeHelpers.js');
var Settings = require('./../../includes/settings.js');

// Testcases
var TestCases = {
    FC_Login: require('./../../testcases/FC/login.js'),
    FC_Form: require('./../../testcases/FC/forms.js'),
    FC_Publisher: require('./../../testcases/FC/publisher.js'),
    FC_Mask: require('./../../testcases/FC/validation_mask.js'),
    FC_Script: require('./../../testcases/FC/validation_script.js'),
    FP_Login: require('./../../testcases/FP/login.js'),
    FP_Company: require('./../../testcases/FP/company.js'),
    FP_Forms: require('./../../testcases/FP/forms.js'),
    FP_Packages: require('./../../testcases/FP/packages.js'),
    TestRail: require('./../../testcases/testrailmark.js'),
};

module.exports = {

    before: function (browser) {
        browser.globals.CLI = CLI;
        browser.globals.Helpers = Helpers;
        browser.globals.TreeHelpers = TreeHelpers;
        browser.globals.Settings = Settings;
        browser.resizeWindow(1400, 800);
    },

    after: function (browser) {
        // it's doesn't work because of redirec to TC
        // TestCases.TestRail['Push result to TestRail'](browser, TestCases.FC_Form['Create a form'], 21, 'TestCases was run and marcked', 'test was faild');  
        TestCases.TestRail['Push result to TestRail'](browser, 'Create package from form', 27, 'Package from form was created by CA&CU user', 'test was failed, package wasn\'t created');  
        TestCases.TestRail['Push result to TestRail'](browser, 'Create package from form as CA', 27, 'Package from form was created by CA user', 'test was failed, package wasn\'t created');  
    },

// Case 27  Create package from form

    // User = CA&CU
    'Open Forms Publisher for creating': function(browser) {
        browser.globals.url = TestCases.FP_Login['Open FP'](browser);
    },

    'Login to Forms Publisher as CA&CU': function(browser){
        TestCases.FP_Login['Login to FP as some static user'](browser, 'ATF_Company_AdminAndUser')
    },

    'Check if form with name "ATF_Form_for_Packages" is present or create it': function(browser) {
        TestCases.FP_Forms['Find form in a Table'](browser, 'ATF_Form_for_Packages', function(result, formName) {
            if(result == true){
                browser
                .assert.ok(true, "Form present at FP");
            }else if(result == false){

                browser
                .assert.ok(true, "Form is not found at FP");
                TestCases.FP_Login['Logout FP'](browser);
                TestCases.FC_Login['Open FC'](browser);
                TestCases.FC_Login['Login to FC as some static user'](browser, "ATF_Company_AdminAndUser");
                TestCases.FC_Publisher['Check if publisher is present'](browser);
                // Check if mascks with name "ValidationMask_Date" and "ValidationMask_Phonenumber" are present
                TestCases.FC_Mask['Find mask in table or create it'](browser, 'ValidationMask_Date', '00-00-0000');
                TestCases.FC_Mask['Find mask in table or create it'](browser, 'ValidationMask_Phonenumber', '0000000009');
                // Check if script with name "ValidateEmail" is present
                TestCases.FC_Script['Find script in table or create it'](browser, 'ValidateEmail', '');
                TestCases.FC_Form['Find form in a Table'](browser, formName, function(result, formName) {
                    if(result == true){
                        browser
                        .assert.ok(true, "Form present on FC");
                    }else{
                        browser
                        .assert.ok(true, "Form is not present on FC");
// Case 21 is used in this test, but wasn't mark in TestRail
                        TestCases.FC_Form['Create a form'](browser, formName, 'This form is for testing packages');
                    }
                        TestCases.FC_Form['Go to form'](browser, formName);
                        TestCases.FC_Form['Import form'](browser, '/attachments/export-Form_for_Packages.zip');

                    TestCases.FC_Form['Publish form'](browser);
                });        
                browser.globals.url = TestCases.FP_Login['Open FP'](browser);
                TestCases.FP_Login['Login to FP as some static user'](browser, 'ATF_Company_AdminAndUser')
            }else{
                browser
                .assert.ok(false, "Something wrong");
            }
        });
    },

    'Delete package if it is already exist': function(browser) {
        TestCases.FP_Packages['Delete package if it is already exist'](browser, 'ATF_Form_for_Packages');
    },

    'Create package from form' : function(browser) {
        TestCases.FP_Packages['Create a package from form'](browser, 'ATF_Form_for_Packages');
    },

    'Close browser': function(browser) {
        browser.end();
    }, 

    // User = CA
    'Open Forms Publisher for creating once more': function(browser) {
        this['Open Forms Publisher for creating'](browser);
    },

    'Login to Forms Publisher as CA': function(browser){
        TestCases.FP_Login['Login to FP as some static user'](browser, 'ATF_Company_Admin');
    },

    'Delete package if it is already exist once more':function(browser){
        this['Delete package if it is already exist'](browser);
    },

    'Create package from form as CA' : function(browser) {
        this['Create package from form'](browser);
    },

    'Close browser as CA': function(browser) {
        this['Close browser'](browser);
    },
}










