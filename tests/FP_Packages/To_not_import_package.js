/**  @module FE/tests/regression */
/**
 * Created by N.Shamborovska on 29.11.17.
 * File name: To_not_import_package.js
 * Full link for test: tests/FP_Packages/To_not_import_package.js
 */

// Modules
var CLI = require('./../../includes/cli.js');
var Helpers = require('./../../includes/helpers.js');
var TreeHelpers = require('./../../includes/treeHelpers.js');
var Settings = require('./../../includes/settings.js');

// Testcases
var TestCases = {
    FP_Login: require('./../../testcases/FP/login.js'),
    FP_Packages: require('./../../testcases/FP/packages.js'),
    TestRail: require('./../../testcases/testrailmark.js'),
};

module.exports = {

    before: function (browser) {
        browser.globals.CLI = CLI;
        browser.globals.Helpers = Helpers;
        browser.globals.TreeHelpers = TreeHelpers;
        browser.globals.Settings = Settings;
        browser.resizeWindow(1400, 800);
    },

    after: function (browser) {
        TestCases.TestRail['Push result to TestRail'](browser, 'Check if error message is displaed', 34, 'Error message was desplayed. Tested by CA&CU user', 'Error message wasn\'t desplayed');  
    },

// Case 34 To not import package

    // User = CA&CU
    'Open Forms Publisher for creating': function(browser) {
        browser.globals.url = TestCases.FP_Login['Open FP'](browser);
    },

    'Login to Forms Publisher as CA&CU': function(browser){
        TestCases.FP_Login['Login to FP as some static user'](browser, 'ATF_Company_AdminAndUser')
    },

    'Try to import package without downloaded file': function(browser) {
        TestCases.FP_Packages['Import package from archive'](browser, 'empty');
    },

    'Check if error message is displaed': function(browser) {
        browser
        .waitForElementVisible('.modal-body #main_import_package_form .row em', browser.globals.defaultTimeout)
        .assert.containsText('.modal-body #main_import_package_form .row em', 'Please select package archive to import');
    },

    'Close browser': function(browser) {
        browser.end();
    }, 
}
